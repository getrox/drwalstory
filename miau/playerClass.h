const int _width = 600;
const int _height = 600;
const int _groundLevel = (int)(_height/2);
const int _groundWidth = (int)(2*_width/5);
const int _gapWidth = 100;
const int _maxJump = 40;

class player
{
public:
	player();
	player(int w, int h);
	player(ALLEGRO_BITMAP *img);
	~player();

	void set_x(int x);
	void set_y(int y);
	int get_x();
	int get_y();
	int get_width();
	int get_height();
	ALLEGRO_BITMAP* get_image();

	bool isOutsideOfTheBox();
	void move(ALLEGRO_EVENT ev);

private:
	int width, height;
	//coordinates
	int x, y;
	// move
	int xDirection, yDirection;
	bool falling; int midairFor, maxJump;
	ALLEGRO_BITMAP* image;

};

///////////////////////////////////////methods////////////////////////////////////////////////
player::player()
{	xDirection=0; yDirection=0;
	width=2; height=3;
	falling=false; maxJump=_maxJump;
	this->image=al_create_bitmap(width,height);
	//coloring the player
	al_set_target_bitmap(image);
	al_clear_to_color(al_map_rgb(218,112,214));

	// player's coordinates
	x=0;
	y=_height-_groundLevel;

}

player::player(int w, int h)
{
	xDirection=0; yDirection=0;
	width=w;
	height=h;
	falling=false; maxJump=_maxJump;
	std::cout<< width;
	this->image=al_create_bitmap(width,height);
	//coloring the player
	al_set_target_bitmap(image);
	al_clear_to_color(al_map_rgb(218,112,214));

	// player's coordinates
	x=0;
	y=_height-_groundLevel;
}

player::player(ALLEGRO_BITMAP* img)
{
	xDirection=0; yDirection=0;
	this->image=img;
	width= al_get_bitmap_width(image);
	height= al_get_bitmap_height(image);
	falling=false; maxJump=_maxJump;
	// player's coordinates
	x=0;
	y=_height-_groundLevel;

}
player::~player()
{
		al_destroy_bitmap(image);
}

void player::set_x(int x)
{
	this->x = x;
}

void player::set_y(int y)
{
	this->y = y;
}

int player::get_x()
{
	return this->x;
}

int player::get_y()
{
	return this->y;
}

int player::get_width()
{
	return this->width;
}

int player::get_height()
{
	return this->height;
}

ALLEGRO_BITMAP* player::get_image()
{
	return this->image;
}

void player::move(ALLEGRO_EVENT ev)
{
	if (y==_height-_groundLevel-height) 						// if player is on the ground level he's not falling
	{
		if (x<= _groundWidth || x>= _groundWidth+_gapWidth-width)
		{
			falling=false;
			yDirection=0;
		}
		else
		{
			falling=true;
			yDirection=1;
		}
	}
	else if (y<_height-_groundLevel-height)
	{
		if (midairFor==maxJump) yDirection=1; 					// if p. reaches max jump -> he starts falling DOWN
		if (yDirection==-1) midairFor++;
	}

	else
	{
		if (x==_groundWidth) xDirection=1;
		else if (x== _groundWidth + _gapWidth - width) xDirection=-1;
	}

	if (!falling){
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			if (ev.keyboard.keycode==ALLEGRO_KEY_RIGHT) xDirection=1;
			else if(ev.keyboard.keycode==ALLEGRO_KEY_LEFT) xDirection=-1;
			else if(ev.keyboard.keycode==ALLEGRO_KEY_SPACE)
			{
				yDirection=-1;
				falling=true;
				midairFor=0;
			}
		}
		else if(ev.type == ALLEGRO_EVENT_KEY_UP)
			{
				if ((ev.keyboard.keycode==ALLEGRO_KEY_LEFT || ev.keyboard.keycode==ALLEGRO_KEY_RIGHT)) xDirection=0;
				//else if (ev.keyboard.keycode==ALLEGRO_KEY_UP) yDirection=0;
			}

	}
	x+=xDirection;
	y+=yDirection;
}

bool player::isOutsideOfTheBox()
{
	if (x>=0 && x<=_width-width && y>=0 && y<=_height-height) return false;
	else return true;
}
