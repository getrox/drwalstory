#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "playerClass.h"
int _groundWidthNumber;
int _groundHeightNumber;

void paintTheGround(ALLEGRO_BITMAP* ground1, ALLEGRO_BITMAP* ground2, ALLEGRO_DISPLAY* display1);

int main(int argc, char *argv[])
{
	al_init();
	al_init_image_addon();
	ALLEGRO_DISPLAY *display1 = NULL;
	ALLEGRO_BITMAP *ground1 = NULL;
	ALLEGRO_BITMAP *ground2 = NULL;
	ALLEGRO_EVENT_QUEUE *queue = NULL;

	al_install_keyboard();

	display1=al_create_display(_width,_height);
	player kitten(al_load_bitmap("kotek.png"));
	//player kitten(100,120);
	//player kitten;

	queue= al_create_event_queue();
	al_register_event_source(queue, al_get_display_event_source(display1));
	al_register_event_source(queue, al_get_keyboard_event_source());

	ALLEGRO_EVENT ev1;
	ALLEGRO_TIMEOUT timeout1;

	/*// creating bitmaps
	ground1 = al_create_bitmap(_groundWidth, _groundLevel);
	ground2 = al_create_bitmap(_width-_groundWidth-_gapWidth, _groundLevel);
	*/
	ground1 = al_load_bitmap("tehground.png");
	ground2 = al_load_bitmap("tehground2.png");

	//y-= _playerHeight;
	kitten.set_y(kitten.get_y() - kitten.get_height());

	/*// coloring the ground
	al_set_target_bitmap(ground1);
	al_clear_to_color(al_map_rgb(0,0,0));
	al_set_target_bitmap(ground2);
	al_clear_to_color(al_map_rgb(0,0,0));
	*/

	int margin_x=_groundWidth%al_get_bitmap_width(ground1), margin_y=_groundLevel%al_get_bitmap_height(ground1);
	while(1)
	{
		al_init_timeout(&timeout1, 0.007);
		bool get_event = al_wait_for_event_until(queue, &ev1 , &timeout1);
		paintTheGround(ground1, ground2, display1);
		if (kitten.isOutsideOfTheBox() || (get_event && (ev1.type== ALLEGRO_EVENT_DISPLAY_CLOSE || ev1.keyboard.keycode==ALLEGRO_KEY_ESCAPE ))) break;
		kitten.move(ev1);
		al_draw_bitmap(kitten.get_image(), kitten.get_x(), kitten.get_y(), 0);
		al_flip_display();
	}

	al_destroy_display(display1);
	al_destroy_bitmap(ground1);
	al_destroy_bitmap(ground2);
	al_destroy_event_queue(queue);
	return 0;
}

///////////////////////////////////////functions/////////////////////////////////////////////////////

void paintTheGround(ALLEGRO_BITMAP* ground1, ALLEGRO_BITMAP* ground2, ALLEGRO_DISPLAY* display1)
{
	int margin_x=_groundWidth%al_get_bitmap_width(ground1), margin_y=_groundLevel%al_get_bitmap_height(ground1);
	// coloring background
	al_set_target_bitmap(al_get_backbuffer(display1));
	al_clear_to_color(al_map_rgb(115, 206, 235));

	_groundWidthNumber=(int)(_groundWidth/al_get_bitmap_width(ground1));
	_groundHeightNumber=(int)(_groundLevel/al_get_bitmap_height(ground1));
	for (int i=0; i<_groundWidthNumber; i++)
		al_draw_bitmap(ground2,i*al_get_bitmap_width(ground2)-margin_x,_height-_groundLevel+margin_y,0);
	for (int i=1; i<_groundHeightNumber; i++){
		for (int j=0; j<_groundWidthNumber; j++)
		{
			al_draw_bitmap(ground1, j*al_get_bitmap_width(ground1)-margin_x, i*al_get_bitmap_height(ground1)+(_height-_groundLevel),0);
		}
	}

	_groundWidthNumber=(int)((_width-_groundWidth+_gapWidth)/al_get_bitmap_width(ground1));
	for (int i=0; i<_groundWidthNumber; i++)
		al_draw_bitmap(ground2,_groundWidth + _gapWidth + i*al_get_bitmap_width(ground2),_height-_groundLevel+margin_y,0);
	for (int i=1; i<_groundHeightNumber; i++){
		for (int j=0; j<_groundWidthNumber; j++)
		{
			al_draw_bitmap(ground1,_groundWidth+_gapWidth + j*al_get_bitmap_width(ground1), i*al_get_bitmap_height(ground1)+(_height-_groundLevel),0);
		}
	}

	//al_draw_bitmap(ground1, 0, _height-_groundLevel, 0);
	//al_draw_bitmap(ground2, _groundWidth+_gapWidth, _height-_groundLevel, 0);
}
