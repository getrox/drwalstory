#include <stdio.h>
#include <allegro5/allegro.h>
#include <cstdlib>
#include <ctime>
//#include <vector>
const float FPS = 60;
const int SCREEN_W = 800;
const int SCREEN_H = 600;
const int THING_SIZE_W = 16;
const int THING_SIZE_H = 16;
const float UNIT = 4.0;
const float GRAVITY = 2.0;
const int N = 6; //numbers of blocks
const int M = 1000; //numbers of raindrops

enum MYKEYS {
	KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT
};

class Drwal {
public:
	int x;
	int y;
	int w;
	int h;
	ALLEGRO_BITMAP *e;
};

class Blocks {
public:
	int x;
	int y;
	int w;
	int h;
	ALLEGRO_BITMAP *e;
};

class Drops {
public:
	int x;
	int y;
	int dim;
	ALLEGRO_BITMAP *e;
};

bool collision(float first_x, float first_y, int first_w, int first_h,
		float second_x, float second_y, int second_w, int second_h);
int main(int argc, char **argv) {
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	bool key[4] = { false, false, false, false };
	bool redraw = true;
	bool doexit = false;
	bool jump = false;
	Blocks something[N];
	Drops rain[M];
	Drwal thing;
	int i;
	bool detection;
	//std::vector<MyObject> table;
	//table.push_back(obj);

	for (i = 0; i < N; i++) //position and large of blocks
	{
		something[i].x = 20.0 + (150.0) * i;
		something[i].y = 100.0 + (100.0) * i;
		something[i].w = 100;
		something[i].h = 24;
	}

	thing.w = THING_SIZE_W; //our Drwal
	thing.h = THING_SIZE_H;
	thing.x = (something[0].x + something[0].w) / 2.0;
	thing.y = something[0].y / 2.0;

	srand(time(NULL));
	for (i = 0; i < M; i++) {
		rain[i].x = rand() % SCREEN_W;
		rain[i].y = rand() % SCREEN_H;
		rain[i].dim = 1.0;
	}

	if (!al_init()) {
		fprintf(stderr, "initalize = fail!\n");
		return -1;
	}
	if (!al_install_keyboard()) {
		fprintf(stderr, "initalize keyboard = fail!\n");
		return -1;
	}
	timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		fprintf(stderr, "create timer = fail!\n");
		return -1;
	}
	display = al_create_display(SCREEN_W, SCREEN_H);
	if (!display) {
		fprintf(stderr, "create display = fail!\n");
		al_destroy_timer(timer);
		return -1;
	}
	thing.e = al_create_bitmap(thing.w, thing.h);
	if (!thing.e) {
		fprintf(stderr, "create thing bitmap = fail!\n");
		al_destroy_display(display);
		al_destroy_timer(timer);
		return -1;
	}

	for (i = 0; i < N; i++) {
		something[i].e = al_create_bitmap(something[i].w, something[i].h);
		if (!something[i].e) {
			fprintf(stderr, "create something bitmap = fail!\n");
			al_destroy_display(display);
			al_destroy_timer(timer);
			return -1;
		}
	}

	for (i = 0; i < M; i++) {
		rain[i].e = al_create_bitmap(rain[i].dim, rain[i].dim);
		if (!rain[i].e) {
			fprintf(stderr, "create rain bitmap = fail!\n");
			al_destroy_display(display);
			al_destroy_timer(timer);
			return -1;
		}
	}

	al_set_target_bitmap(thing.e);
	al_clear_to_color(al_map_rgb(0, 128, 0));
	al_set_target_bitmap(al_get_backbuffer(display));

	for (i = 0; i < N; i++) {
		al_set_target_bitmap(something[i].e);
		al_clear_to_color(al_map_rgb(122, 0, 0));
		al_set_target_bitmap(al_get_backbuffer(display));
	}
	for (i = 0; i < M; i++) {
		al_set_target_bitmap(rain[i].e);
		al_clear_to_color(al_map_rgb(0, 255, 255));
		al_set_target_bitmap(al_get_backbuffer(display));
	}

	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "create event_queue = fail!\n");
		al_destroy_bitmap(thing.e);
		for (i = 0; i < N; i++)
			al_destroy_bitmap(something[i].e);
		for (i = 0; i < M; i++)
			al_destroy_bitmap(rain[i].e);
		al_destroy_display(display);
		al_destroy_timer(timer);
		return -1;
	}

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();
	al_start_timer(timer);

	while (!doexit) {
		ALLEGRO_EVENT event;
		al_wait_for_event(event_queue, &event);

		if (event.type == ALLEGRO_EVENT_TIMER) {
			if (key[KEY_UP] && thing.y >= UNIT) {
				detection = false;
				i = 0;
				while (i < N && !detection) {
					detection = collision(thing.x, thing.y - UNIT, thing.w,
							thing.h, something[i].x, something[i].y,
							something[i].w, something[i].h);
					++i;
				}
			}
			if (key[KEY_DOWN] && thing.y <= SCREEN_H - thing.h - UNIT) {
				detection = false;
				i = 0;
				while (i < N && !detection) {
					detection = collision(thing.x, thing.y + UNIT, thing.w,
							thing.h, something[i].x, something[i].y,
							something[i].w, something[i].h);
					++i;
				}
				if (!detection)
					thing.y += UNIT / 2;
				else
					jump = false;
			}
			if (key[KEY_LEFT] && thing.x >= UNIT) {
				detection = false;
				i = 0;
				while (i < N && !detection) {
					detection = collision(thing.x - UNIT, thing.y, thing.w,
							thing.h, something[i].x, something[i].y,
							something[i].w, something[i].h);
					++i;
				}
				if (!detection) {
					thing.x -= UNIT / 2;
				}

			}
			if (key[KEY_RIGHT] && thing.x <= SCREEN_W - thing.w - UNIT) {
				detection = false;
				i = 0;
				while (i < N && !detection) {
					detection = collision(thing.x + UNIT, thing.y, thing.w,
							thing.h, something[i].x, something[i].y,
							something[i].w, something[i].h);
					++i;
				}
				if (!detection)
					thing.x += UNIT / 2;
			}
			if (thing.y <= SCREEN_H - thing.h - UNIT + GRAVITY) {
				detection = false;
				i = 0;
				while (i < N && !detection) {
					detection = collision(thing.x, thing.y + UNIT - GRAVITY,
							thing.w, thing.h, something[i].x, something[i].y,
							something[i].w, something[i].h);
					++i;
				}
				if (!detection) {
					thing.y += GRAVITY;

				}
			}
			if (jump) {
		        thing.y -= 20;
				jump = false;
			}
			if (key[KEY_UP] && detection)
				jump = true;
			else {
				jump = false;
			}
			redraw = true;
		} else if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			break;
		else if (event.type == ALLEGRO_EVENT_KEY_DOWN)
			switch (event.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = true;
				break;
			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = true;
				break;
			case ALLEGRO_KEY_LEFT:
				key[KEY_LEFT] = true;
				break;
			case ALLEGRO_KEY_RIGHT:
				key[KEY_RIGHT] = true;
				break;
			}
		else if (event.type == ALLEGRO_EVENT_KEY_UP)
			switch (event.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = false;
				break;
			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = false;
				break;
			case ALLEGRO_KEY_LEFT:
				key[KEY_LEFT] = false;
				break;
			case ALLEGRO_KEY_RIGHT:
				key[KEY_RIGHT] = false;
				break;
			case ALLEGRO_KEY_ESCAPE:
				doexit = true;
				break;
			}
		for (i = 0; i < M; i++) {
			redraw = true;
			if (rain[i].y <= SCREEN_H)
				rain[i].y += (rand() % 5 + 1) / 2 * UNIT;
			else {
				rain[i].x = rand() % SCREEN_W;
				rain[i].y = rand() % SCREEN_H / 2;
			}
		}
		if (redraw && al_is_event_queue_empty(event_queue)) {
			redraw = false;
			al_clear_to_color(al_map_rgb(0, 0, 0));
			al_draw_bitmap(thing.e, thing.x, thing.y, 0);
			for (i = 0; i < N; i++)
				al_draw_bitmap(something[i].e, something[i].x, something[i].y,
						0);
			for (i = 0; i < M; i++)
				al_draw_bitmap(rain[i].e, rain[i].x, rain[i].y, 0);
			al_flip_display();
		}
	}

	al_destroy_bitmap(thing.e);
	for (i = 0; i < N; i++)
		al_destroy_bitmap(something[i].e);
	for (i = 0; i < M; i++)
		al_destroy_bitmap(rain[i].e);
	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
}

bool collision(float first_x, float first_y, int first_w, int first_h,
		float second_x, float second_y, int second_w, int second_h) {
	if (first_x > second_x + second_w - 1 || first_y > second_y + second_h - 1
			|| second_x > first_x + first_w - 1 || second_y > first_y + first_h
			- 1)
		return false;
	else
		return true;
}
