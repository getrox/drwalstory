#ifndef MAIN_LOOP_H
#define MAIN_LOOP_H

#include "public.h"

int menuLoop(ALLEGRO_DISPLAY * display); //Main menu.
int collisionVectorLoop(ALLEGRO_DISPLAY * display); //For testing vector collision
int collisionPixelLoop(ALLEGRO_DISPLAY* display); //For testing pixel collision

#endif
