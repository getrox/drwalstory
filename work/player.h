#ifndef PLAYER_H
#define PLAYER_H

#include "public.h"
#include "collision.h"
class Player
{
public:
	ALLEGRO_BITMAP *drwal_image;
	float pos_x, pos_y, s_x, s_y; //For player position, and speed
	Player();
	BBox drwal_box; //Collision box
	bool jump;

	void sterowanie(bool *ster, MapMask *map); //Player movement, AND collision!
	void sterowanie2(bool *ster, MapMask *map);
	void sterowanie3(MapMask *map);
	void debug_ster(bool *ster, MapMask *map);

	void acceleration(bool *ster); //Player acceleration, collects info from ster array
	void pokaz();

};

#endif
