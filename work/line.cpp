#include "line.h"
#include <boost/geometry/geometry.hpp>

Line::Line()
{
	x1=0;
	y1=0;
	x2=0;
	y2=0;
}

Line::Line(float a, float b, float c, float d)
{
	this->x1 = a;
	this->y1 = b;
	this->x2 = c;
	this->y2 = d;
	Point p1, p2;
	boost::geometry::assign_values(p1, x1, y1);
	boost::geometry::assign_values(p2, x2, y2);
	boost::geometry::append(geometry, p1);
	boost::geometry::append(geometry, p2);
	std::cout << boost::geometry::dsv(geometry) << std::endl;
}

BoostLine Line::getGeometry()
{

	return geometry;
}
