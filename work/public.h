#ifndef DRWAL_H
#define DRWAL_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <iostream>
#include <math.h>
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#define FPS (60.0)

typedef boost::geometry::model::d2::point_xy<float> Point;

#endif
