#include "public.h"
#include "collision.h"
#include "player.h"
#include "addons.h"
#include "loop.h"

#include "MrFauxPas_loop.h"

int main(int argc, char ** argv)
{
	ALLEGRO_DISPLAY *display = NULL;
	int loops = 0;

	al_init_primitives_addon();
	al_init_image_addon();

	if (!al_init())
		return -1;

	display = al_create_display(800, 640);
	if (!display)
		return -2;

	if (!al_install_keyboard())
		return -3;

	if (!al_install_mouse())
		return -4;

	loops = menuLoop(display);
	if (loops == 1)
		loops = collisionPixelLoop(display);
	else if (loops == 2)
		loops = collisionVectorLoop(display);
	else if (loops == 3)
		loops = mateuchGeometry(display);

	al_destroy_display(display);

	return loops;
}
