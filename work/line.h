/*
 * line.h
 *
 *  Created on: 2012-04-29
 *      Author: getrox
 */

#ifndef LINE_H_
#define LINE_H_

#include "public.h"

#include <boost/geometry/geometries/linestring.hpp>

typedef boost::geometry::model::linestring<Point> BoostLine;

/**
 * @brief
 * Implementation of a line for collision purposes.
 * Includes geometry for boost::geometry::intersecs function.
 */
class Line
{
private:
	float x1, x2, y1, y2;
public:
	Line();
	/**
	 * @param[x1] First point x value
	 * @param[y1] First point y value
	 * @param[x2] Second point x value
	 * @param[y2] Second point y value
	 */
	Line(float, float, float, float);
	BoostLine getGeometry();
	BoostLine geometry;
};

#endif /* LINE_H_ */
