/*
 * polygon.cpp
 *
 *  Created on: 2012-05-01
 *      Author: getrox
 */

#include "polygon.h"

Polygon::Polygon()
{
	closed = false;
}

Polygon::Polygon(const std::vector<Point> &tab)
{
	geometry.assign(tab.begin(), tab.end());
	int last = tab.size() - 1;
	if (geometry[0].x() == geometry[last].x()
			&& geometry[0].y() == geometry[last].y())
		closed = true;
	else
		closed = false;
}

Polygon::Polygon(const Polygon & old)
{
	geometry.clear();
	geometry.assign(old.geometry.begin(), old.geometry.end());
	closed = old.closed;
}

Polygon Polygon::operator=(const Polygon & old)
{
	static Polygon left(old);
	return left;
}
void Polygon::draw(const ALLEGRO_COLOR &color)
{
	int size = geometry.size();

	for (int i = 1; i < size; i++)
		al_draw_line(geometry[i - 1].x(), geometry[i - 1].y(), geometry[i].x(),
				geometry[i].y(), color, 1);
	for (int i = 0; i < size; i++)
		al_draw_filled_circle(geometry[i].x(), geometry[i].y(), 1.0, color);
}

void Polygon::drawPoint(const int &p, const ALLEGRO_COLOR &color)
{
	al_draw_filled_circle(geometry[p].x(), geometry[p].y(), 5.0, color);
}

void Polygon::move(float const x = 0, float const y = 0)
{
	int size = geometry.size();
	for (int i = 0; i < size; i++)
		movePoint(i, x, y);
}
void Polygon::moveAbs(float const x, float const y)
{
	Point center;
	boost::geometry::centroid(geometry, center);

	move(x - center.x(), y - center.y());

}

void Polygon::movePoint(const int &p, const float &x, const float &y)
{
	geometry[p].x(geometry[p].x() + x);
	geometry[p].y(geometry[p].y() + y);
}

void Polygon::movePointAbs(const int &p, const float &x, const float &y)
{
	geometry[p].x(x);
	geometry[p].y(y);
}

void Polygon::removePoint(const int &which)
{
	std::deque<Point>::iterator it;
	it = geometry.begin();
	geometry.erase(it + which);
}

void Polygon::rotate(float const &rot)
{
	Point center;
	boost::geometry::centroid(geometry, center);

	//Rotation. I have no idea what I'm doing.
	BoostPolygon tmpGeometry;
	boost::geometry::strategy::transform::rotate_transformer<Point, Point,
			boost::geometry::degree> rotate(rot);
	move(-center.x(), -center.y());

	int size = geometry.size();
	for (int i = 0; i < size; i++)
		boost::geometry::transform(geometry[i], geometry[i], rotate);

	move(center.x(), center.y());

}

void Polygon::close()
{
	if (geometry.size() > 0)
	{
		geometry.push_back(geometry[0]);
		closed = true;
	}
}

void Polygon::open()
{
	if (geometry.size() > 0)
	{
		geometry.pop_back();
		closed = false;
	}
}

void Polygon::addPoint(const Point &p, const bool &end)
{
	if (end)
	{
		int size = geometry.size() - 1;
		if (size == -1)
		{
			geometry.push_back(p);
			geometry.push_back(p);
		}

		if (closed)
		{
			geometry[size] = p;
			geometry.push_back(geometry[0]);
		}
		else
			geometry.push_back(p);
	}
	else
	{
		int size = geometry.size() - 1;
		if (size == -1)
			geometry.push_front(p);
		geometry.push_front(p);
		if (closed)
			geometry[size + 1] = geometry[0];
	}
}
void Polygon::insertPoint(const int &next)
{
	Point inserted;
	std::deque<Point>::iterator it;
	it = geometry.begin();
	//If we add something before the begining of a closed polygon:
	if (closed && next == 0)
	{
		int before = geometry.size() - 1;
		inserted.x((geometry[0].x() + geometry[before - 1].x()) / 2);
		inserted.y((geometry[0].y() + geometry[before - 1].y()) / 2);
		geometry.insert(it + before, inserted);
	}
	//And for the rest:
	else if (next != 0)
	{
		inserted.x((geometry[next].x() + geometry[next - 1].x()) / 2);
		inserted.y((geometry[next].y() + geometry[next - 1].y()) / 2);
		geometry.insert(it + next, inserted);
	}
}

Polygon* Polygon::divide(const int &p)
{
	//Calculates sizes: For existing Polygon, and for two new Polygons.
	int size_current = geometry.size() - 1;
	int size_part1 = p - 1;
	if (size_part1 < 0)
		size_part1 = 0;
	int size_part2 = size_current - p;
	using namespace std;
	cout << size_current << " " << size_part1 << " " << size_part2 << " " << p
			<< endl;
	Polygon *part2;
	if (size_part2 > 0)
	{
		part2 = new Polygon;
		part2->closed = closed;
	}
	else
		part2 = 0;

	//Add Points from p to geometry.size()-1 to the second part.
	for (int i = 0; i < size_part2; i++)
	{
		part2->geometry.push_front(geometry[size_current - i]);
	}
	//Resize first part.
	geometry.resize(size_part1);

	//If size of our first part (*this part) is 0, we should remove ourselves. Or something.
	return part2;
}

bool Polygon::isCollision(const Polygon &other)
{
	return boost::geometry::intersects((*this).geometry, other.geometry);
}

void Polygon::moveIfCan(const float &x, const float &y, const float &rot,
		const std::vector<Polygon*> &other)
{
	Polygon tester(*this);
	tester.move(x, y);
	tester.rotate(rot);
	bool collision = false;
	for (int i = 0; i < other.size() && !collision; i++)
		collision = collision || tester.isCollision(*other[i]);
	if (!collision)
	{
		move(x, y);
		rotate(rot);
	}
}
void Polygon::movePointIfCan(const float &x, const float &y, const int &p,
		const Polygon &other)
{
	int size = geometry.size() - 1;
	if (p == -1 || p > size)
		return;

	Polygon tester(*this);
	tester.movePointAbs(p, x, y);
	if (closed && p == 0)
	{
		tester.movePointAbs(size, x, y);
		if (!tester.isCollision(other))
		{
			movePointAbs(p, x, y);
			movePointAbs(size, x, y);
		}
	}
	else if (!tester.isCollision(other))
		movePointAbs(p, x, y);
}

void Polygon::changeIfCan(bool change_closed, Point *add, bool at_end,
		int remove, int knife, const Polygon &other)
{
	Polygon tester(*this);
	if (change_closed)
	{
		if (closed)
			open();
		else
		{
			tester.close();
			if (!tester.isCollision(other))
				close();
		}
	}
	else if (add != NULL)
	{
		tester.addPoint(*add, at_end);
		if (!tester.isCollision(other))
			addPoint(*add, at_end);
	}
	else if (remove != -1)
	{
		int size = tester.geometry.size() - 1;
		;
		tester.removePoint(remove);
		if (remove == 0 && closed && size > 0)
			tester.geometry[size] = tester.geometry[0];

		if (!tester.isCollision(other))
		{
			removePoint(remove);
			if (remove == 0 && closed && size > 0)
				tester.geometry[size] = tester.geometry[0];
		}
	}
	else if (knife != -1)
	{
		insertPoint(knife);
	}
}

bool Polygon::mouseCollision(const float &x, const float &y,
		const float &radius)
{
	if (geometry.size() == 0)
		return false;
	Point mouse(x, y);

	float dist = boost::geometry::distance(mouse, geometry);
	if (dist < radius)
		return true;
	else
		return false;
}

int Polygon::mousePointCollision(const float &x, const float &y,
		const float &radius)
{
	int size = geometry.size();
	for (int i = 0; i < size; i++)
	{
		//If the distance between a point and cursor is less than 5, mouse points to it..
		if ((x - geometry[i].x()) * (x - geometry[i].x())
				+ (y - geometry[i].y()) * (y - geometry[i].y())
				< radius * radius)
			return i;
	}
	return -1;
}
