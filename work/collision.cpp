#include "public.h"
#include "collision.h"
#include "addons.h"
#include <math.h>

OldPoint::OldPoint(float nx, float ny)
{
	x = nx;
	y = ny;
}

Vector::Vector()
{
	x = y = bx = by = i = j = lenght = 0;
}

Vector::Vector(float nx, float ny)
{
	bx = 0;
	by = 0;

	x = nx;
	y = ny;
	lenght = sqrt(x * x + y * y);
	if (lenght != 0)
	{
		i = x / lenght;
		j = y / lenght;
	}
	else
		i = j = 0;
}

Vector::Vector(OldPoint& first, OldPoint& latter, int flag = 0)
{
	if (flag == 0)
	{
		x = latter.x - first.x;
		y = latter.y - first.y;
		bx = first.x;
		by = first.y;

		lenght = sqrt(x * x + y * y);
		if (lenght != 0)
		{
			i = x / lenght;
			j = y / lenght;
		}
		else
			i = j = 0;
	}
	else if (flag == 1)
	{
		x = latter.y - first.y;
		y = -(latter.x - first.x);
		bx = (first.x + latter.x) / 2;
		by = (first.y + latter.y) / 2;

		lenght = sqrt(x * x + y * y);
		if (lenght != 0)
		{
			i = x / lenght;
			j = y / lenght;
		}
		else
			i = j = 0;
	}
}

Vector::Vector(Vector & a, Vector & b)
{
	float part;
	part = (a.x * b.x + a.y * b.y) / (b.x * b.x + b.y * b.y);
	x = part * b.x;
	y = part * b.y;

	bx = b.bx + b.x;
	by = b.by + b.y;
	lenght = sqrt(x * x + y * y);
	if (lenght != 0)
	{
		i = x / lenght;
		j = y / lenght;
	}
	else
		i = j = 0;
}

void Circle::draw()
{
	al_draw_circle(this->o.x, this->o.y, this->r, al_map_rgb(0xFF, 0x14, 0x93),
			1);
}

void Vector::draw()
{
	al_draw_line(bx, by, bx + 50 * i, by + 50 * j, al_map_rgba(0, 0, 0, 200),
			1);
}

OldPoint& Vector::getBeginingPoint()
{
	static OldPoint p;
	p.x = this->bx;
	p.y = this->by;
	return p;
}

OldPoint& Vector::getEndingPoint()
{
	static OldPoint p;
	p.x = this->x + this->bx;
	p.y = this->y + this->by;
	return p;
}
Triangle::Triangle(OldPoint first, OldPoint second, OldPoint third)
{
	p[0] = first;
	p[1] = second;
	p[2] = third;
}
BBox2::BBox2(OldPoint first, OldPoint second) //Builds a bouding box using two points, stores 4.
{
	is_rotated = false;
	p[0].x = first.x;
	p[0].y = first.y;

	p[3].x = first.x;
	p[3].y = second.y;

	p[1].x = second.x;
	p[1].y = first.y;

	p[2].x = second.x;
	p[2].y = second.y;

	p[4].x = (p[0].x + p[1].x) / 2;
	p[4].y = (p[0].y + p[3].y) / 2;
}
BBox2& BBox2::operator=(BBox2 &b)
{
	for (int i = 0; i < 5; i++)
	{
		this->p[i] = b.p[i];
	}
	return *this;
}
float BBox2::getWidth()
{
	return p[1].x - p[0].x;
}
float BBox2::getHeight()
{
	return p[1].y - p[2].y;
}
bool BBox2::isPointInBox(OldPoint& pt)
{
	return pt.x >= p[0].x && pt.x <= p[1].x && pt.y >= p[2].y && pt.y <= p[1].y;
}
bool BBox2::isCollision(MaskLine& lin)
{
	MaskLine edges[] = {MaskLine(p[0],p[1]), MaskLine(p[1],p[2]), MaskLine(p[2],p[3]), MaskLine(p[3],p[0])};
	for(int i=0;i<4;i++)
	{
		if(collisionLineLine(lin, edges[i]))
			return true;
	}
	return false;

	/*Point tmp = lin.getClosestPoint(p[4]);
	 if(!lin.checkPoint(lin.getClosestPoint(p[4])))
	 {
	 return false;
	 }
	 return isPointInBox(lin.getClosestPoint(p[4]));*/
	//
	//return collisionBBox2MaskLine(*this, lin);
}
bool BBox2::isCollision(BBox2 & box)
{


	 for(int i=0;i<4;i++)
	 {

	 if(isPointInBox(box.p[i]))
	 {
	 return true;
	 }
	 }
	 for(int i=0;i<4;i++)
	 {

	 if(box.isPointInBox(p[i]))
	 {
	 return true;
	 }
	 }
	 return false;
	/*
	Edges tmp = getEdges(*this);
	Edges tmp2 = getEdges(box);
	for(int i=0;i<4;i++)
	{

		for(int j=0;j<4;j++)
		{

			if(collisionLineLine(tmp.v1[i], tmp2.v1[j]))
			{
				return true;
			}
		}
	}
	return false;*/
}
Edges& getEdges(BBox2 & box)
{
	static Edges output =
	{ MaskLine(box.p[0], box.p[1]), MaskLine(box.p[1], box.p[2]), MaskLine(
			box.p[2], box.p[3]), MaskLine(box.p[3], box.p[0]) };
	return output;
}
bool Triangle::isCollision(BBox2& box)
{
	MaskLine lines[] =
	{ MaskLine(p[0], p[1]), MaskLine(p[1], p[2]), MaskLine(p[0], p[2]) };
	if (box.isCollision(lines[0]))
		return true;
	else if (box.isCollision(lines[1]))
		return true;
	else if (box.isCollision(lines[2]))
		return true;
	else
		return false;

}

bool BBox2::collisionRotatedBBox(BBox2& box)
{
	MaskLine lines[] =
	{ MaskLine(p[3], p[2]), MaskLine(p[3], p[0]), MaskLine(p[1], p[2]),
			MaskLine(p[1], p[0]) };
	if (box.isCollision(lines[0]))
		return true;
	else if (box.isCollision(lines[1]))
		return true;
	else if (box.isCollision(lines[2]))
		return true;
	else if (box.isCollision(lines[3]))
		return true;
	else
		return false;
}

MaskLine::MaskLine(OldPoint First, OldPoint Latter)
{

	p1 = First;
	p2 = Latter;
	BBox2 tmp = BBox2(First, Latter);
	box = tmp;
	normal = Vector(First, Latter, 1);
}

void MaskLine::draw()
{
	al_draw_line(p1.x, p1.y, p2.x, p2.y, al_map_rgba(200, 0, 0, 100), 1);
}

Line& MaskLine::getLine()
{
	static Line output;
	output.a = (p1.y - p2.y) / (p1.x - p2.x);
	output.b = p2.y - output.a * p2.x;
	return output;
}
bool MaskLine::checkPoint(OldPoint & pt)
{
	float y1 = fabs(p1.y - pt.y);
	float y2 = fabs(p2.y - pt.y);
	float y3 = fabs(p1.y - p2.y);
	float x1 = fabs(p1.x - pt.x);
	float x2 = fabs(p2.x - pt.x);
	float x3 = fabs(p1.x - p2.x);
	return y1 <= y3 && y2 <= y3 && x1 <= x3 && x2 <= x3;
}
Line& MaskLine::getPrependicularLine(OldPoint& pt)
{

	static Line output = getLine();
	output.a = 1 / output.a;
	output.b = pt.y - output.a * pt.x;
	return output;
}

OldPoint& MaskLine::getClosestPoint(OldPoint& pt)
{ //lines[0] = ;
//lines[1] = ;
//lines[2] = ;
	static OldPoint output;
	Line first = getLine();
	Line second = getPrependicularLine(pt);
	output.x = (first.b - second.b) / (second.a - first.a);
	output.y = second.a * output.x + second.b;
	return output;
}
OldPoint& MaskLine::getCommonPoint(MaskLine& lin)
{
	static OldPoint new_point;
	Line l1 = lin.getLine();
	Line l2 = this->getLine();
	new_point.x = (l2.b - l1.b) / (l1.a - l2.a);
	new_point.y = l1.a*new_point.x + l1.b;
	return new_point;

}
void move(BBox2 & box, float x, float y) //move the bouding box. Had problems with returning a new one soo...
{
	//BBox2 n;
	for (int i = 0; i < 5; i++)
	{
		box.p[i].x += x;
		box.p[i].y += y;
		//std::cout << box->p[i].y << std::endl;
	}
	//std::cout << n.p[0].x << " " << n.p[0].y << std::endl;
	//return n;
}

void BBox2::draw() //Draw the rectangle for the BBox2.
{
	int j;
	ALLEGRO_COLOR lecolor = al_map_rgba(255, 255, 255, 100);
	for (int i = 0; i < 4; i++)
	{
		if (i == 3)
			j = 0;
		else
			j = i + 1;
		al_draw_line(p[i].x, p[i].y, p[j].x, p[j].y, lecolor, 1);
	}

	al_draw_pixel(p[4].x, p[4].y, lecolor);
}

BBox2& BBox2::rotate(float delta)
{
	static BBox2 rotated;
	rotated.is_rotated = true;
	rotated.p[4].x = 0;
	rotated.p[4].y = 0;
	rotated.p[0].x = (p[0].x - p[4].x) * cos(delta)
			- (p[0].y - p[4].y) * sin(delta);
	rotated.p[0].y = (p[0].x - p[4].x) * sin(delta)
			+ (p[0].y - p[4].y) * cos(delta);

	std::cout << rotated.p[0].x << " " << rotated.p[0].y << std::endl;

	Vector middleToPoint(rotated.p[0], rotated.p[4]);
	rotated.p[2].x = middleToPoint.x;
	rotated.p[2].y = middleToPoint.y;

	rotated.p[1].x = (p[1].x - p[4].x) * cos(delta)
			- (p[1].y - p[4].y) * sin(delta);
	rotated.p[1].y = (p[1].x - p[4].x) * sin(delta)
			+ (p[1].y - p[4].y) * cos(delta);

	middleToPoint = Vector(rotated.p[1], rotated.p[4]);

	rotated.p[3].x = middleToPoint.x;
	rotated.p[3].y = middleToPoint.y;

	for (int i = 0; i < 5; i++)
	{
		rotated.p[i].x += p[4].x;
		rotated.p[i].y += p[4].y;
	}

	return rotated;
}

MapMask loadBitmap(ALLEGRO_BITMAP* bitmap)
{
	al_lock_bitmap(bitmap, al_get_bitmap_format(bitmap), ALLEGRO_LOCK_READONLY);
	MapMask new_bitmap;
	new_bitmap.size_w = al_get_bitmap_width(bitmap);
	new_bitmap.size_h = al_get_bitmap_height(bitmap);
	std::cout << new_bitmap.size_w << " " << new_bitmap.size_h << std::endl;
	new_bitmap.Mask = new int*[new_bitmap.size_h];
	for (int i = 0; i < new_bitmap.size_h; i++)
	{
		new_bitmap.Mask[i] = new int[new_bitmap.size_w];

		for (int j = 0; j < new_bitmap.size_w; j++)
		{
			if (colorCompare(255, 255, 255, 255, al_get_pixel(bitmap, j, i)))
				new_bitmap.Mask[i][j] = 0;
			else
				new_bitmap.Mask[i][j] = 1;
		}
	}
	al_unlock_bitmap(bitmap);
	return new_bitmap;
}

void c_showBitmap(MapMask *map)
{
	int h = map->size_h, w = map->size_w;
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
			std::cout << map->Mask[i][j] << " ";
		std::cout << std::endl;
	}
}

bool collisionBBoxBBox2(BBox2 & first_box, BBox2 & second_box) //SAT collision for bounding boxes.
{
	//First, you calculate a vector, so you know which quarters of the box you need to check.

	/*Vector diag(second_box.p[4], first_box.p[4]);
	float width_first = abs(first_box.p[1].x - first_box.p[0].x);
	float height_first = abs(first_box.p[1].y - first_box.p[2].y);
	float width_second = abs(second_box.p[1].x - second_box.p[0].x);
	float height_second = abs(second_box.p[1].y - second_box.p[2].y);

	if (abs(diag.x) > (width_first + width_second) / 2) //Now check if the vector lenght is bigger than sums
		return false; //of two halves of the box... oh, ToDo Optimalization of this.

	if (abs(diag.y) > (height_first + height_second) / 2)
		return false;
	//a1 = (Mask)/();
	return true; //If no free from collision axis was found, then there's a collision.
	*/

}

bool collisionRBoxRBox(BBox2 & box1, BBox2 & box2)
{
	float width1, width2, height1, height2;

	width1 = _lenghtPoint(box1.p[0], box1.p[1]);
	width2 = _lenghtPoint(box2.p[0], box2.p[1]);

	height1 = _lenghtPoint(box1.p[1], box1.p[2]);
	height2 = _lenghtPoint(box2.p[1], box2.p[2]);

	Vector v1(box1.p[0], box1.p[1]); //Creating vectors from nonrotated box for projection onto normals
	Vector v2(box1.p[1], box1.p[2]);

	Vector n1(box2.p[0], box2.p[1]); //Creating normals.
	Vector n2(box2.p[1], box2.p[2]);

	float lenght[4]; //Preparing lengths

	//Vectors for projections

	Vector p1(v1, n2);
	Vector p2(v2, n2);

	lenght[0] = p1.lenght + p2.lenght;

	p1 = Vector(v1, n1);
	p2 = Vector(v2, n1);

	lenght[1] = p1.lenght + p2.lenght;

	p1 = Vector(n1, v2);
	p2 = Vector(n2, v2);

	lenght[2] = p1.lenght + p2.lenght;

	p1 = Vector(n1, v1);
	p2 = Vector(n2, v1);

	lenght[3] = p1.lenght + p2.lenght;

	//Getting distance between the middles.

	Vector v3(box1.p[4], box2.p[4]);

	//Projecting them onto second box.
	p1 = Vector(v3, n2);
	p2 = Vector(v3, n1);

	if (p1.lenght > (height2 + lenght[0]) / 2)
		return false;
	if (p2.lenght > (width2 + lenght[1]) / 2)
		return false;

	//Projecting distance betweend the middles on first box.
	p1 = Vector(v3, v2);
	p2 = Vector(v3, v1);

	if (p1.lenght > (height1 + lenght[2]) / 2)
		return false;
	if (p2.lenght > (width1 + lenght[3]) / 2)
		return false;

	return true;

}

bool collisionBBox2MaskLine(BBox2 & box, MaskLine & line) //Quite important
{
	if (!box.isCollision(line.box))
	{
		return false;
	}
	Vector tmp1(box.p[0], box.p[1]);
	Vector tmp2(box.p[1], box.p[2]);
	Vector lol3(box.p[2], box.p[3]);
	Vector lol4(box.p[3], box.p[0]);

	Vector tmp3(line.p1, box.p[4]);
	Vector v1(tmp1, line.normal); //Project box width onto normal
	Vector v2(tmp2, line.normal); //Project box lenght onto normal
	Vector vlol3(lol3, line.normal);
	Vector vlol4(lol4, line.normal);

	std::cout << "Kolizja? " << v1.lenght << " " << vlol3.lenght << "\n" << ": "
			<< v2.lenght << " " << vlol4.lenght << std::endl;
	Vector v3(tmp3, line.normal); //Project the area between the point of the line
	//and bbox center. The point can be any.

	std::cout << "Odleglosc srodka od lini: " << v3.lenght << " "
			<< (v1.lenght + v2.lenght) / 2 << std::endl;

	if (v3.lenght > (v1.lenght + v2.lenght) / 2) //Compare.
		return false;
	else
		return true;
}

bool collisionBBoxMask(BBox & box, MapMask * map)
{
	bool is_collision = false;

	int x1 = box.x1, x2 = box.x2, y1 = box.y1, y2 = box.y2; //For indexing the Mask array.
	int i, j; //lines[0] = ;

	//Checking if the bounding box is outside the array.
	//std::cout << y2 << " " << Map->size_h << std::endl;
	if (x1 < 0)
		x1 = 0;
	if (y1 < 0)
		y1 = 0;

	if (x2 < 0) //This shouldn't happen.
		x2 = 0;
	if (y2 < 0)
		y2 = 0;

	if (x2 >= map->size_w)
		x2 = map->size_w - 1;
	if (y2 >= map->size_h)
		y2 = map->size_h - 1;

	if (x1 >= map->size_w) //And again, this shouldn't happen. No character totally out of the map->
		x1 = map->size_w - 1;
	if (y1 >= map->size_h) //Eventually if any of these is true, Collision = true and return!
		y1 = map->size_h - 1; //... maybe except flying out of the screen in the sky, though.

	i = x1;
	j = y1;

	//std::cout << "Zyje" << std::endl;

	while (i <= x2 && !is_collision) //Checking both up and down border
	{
		//std::cout << "i: " << y1 << " x1: " << y2 << std::endl;
		if (map->Mask[y1][i] == 1 || map->Mask[y2][i])
		{
			is_collision = true;
			//std::cout << "findo "<< j << " " << i << std::endl;
		}
		i++;
	}

	//std::cout << "Zyje2" << std::endl;
	while (j <= y2 && !is_collision) //Checking both left and right borders
	{
		if (map->Mask[j][x1] == 1 || map->Mask[j][x2] == 1)
		{
			is_collision = true;
			//std::cout << "findor "<< j << " " << i << std::endl;
		}
		j++;
	}
	//std::cout << "Zyje3" << std::endl;

	if (!is_collision) //Checking for the whole area if collision not found.
	{
		i = x1 + 1;
		while (i < x2 && !is_collision)
		{
			j = y1 + 1;
			while (j < y2 && !is_collision)
			{
				if (map->Mask[j][i] == 1)
				{
					is_collision = true;
					std::cout << "find " << j << " " << i << std::endl;
				}
				j++;
			}
			i++;
		}
	}
	//std::cout << "Zyje4" << std::endl;
	return is_collision;

}

bool collisionBBoxMask_w_cords(BBox & box, MapMask * map, int max_y, int max_x,
		int min_y, int min_x)
{
	bool is_collision = false;
	bool in_column = false, in_row = false, first_row = true, first_column =
			true;

	int x1 = box.x1, x2 = box.x2, y1 = box.y1, y2 = box.y2; //For indexing the Mask array.
	int i, j;

	min_y = y2; //Making the variables go somewhere.
	min_x = x2; //Make them die!
	max_y = y1; //Get the fuck out of me!
	max_x = x1; //I'm on a fucking boat!

	//Checking if the bounding box is outside the array.

	if (x1 < 0) //hi my name is...
		x1 = 0;
	if (y1 < 0)
		y1 = 0;

	if (x2 < 0) //This shouldn't happen.
		x2 = 0; // what?
	if (y2 < 0)
		y2 = 0; // slim shady!

	if (x2 > map->size_w)
		x2 = map->size_w - 1;
	if (y2 > map->size_h)
		y2 = map->size_h - 1;

	if (x1 > map->size_w) //And again, this shouldn't happen. No character totally out of the map->
		x1 = map->size_w - 1;
	if (y1 > map->size_h) //Eventually if any of these is true, Collision = true and return!
		y1 = map->size_h - 1; //... maybe except flying out of the screen in the sky, though.

	i = x1;
	j = y1;

	while (i <= x2)
	{
		in_column = false;
		j = y1;
		while (j <= y2)
		{
			in_row = false;
			//std::cout << Map->Mask[i][j] << " ";
			if (map->Mask[j][i] == 1)
			{
				is_collision = true;
				in_column = true;
				in_row = true;

			}
			if (in_column)
			{
				if (min_x > i)
					min_x = i;
				if (max_x < i)
					max_x = i;
			}
			if (in_row)
			{
				if (min_y > j)
					min_y = j;
				//first_row = false;
				if (max_y < j)
					max_y = j;
			}

			j++;
		}
		//std::cout << std::endl;
		i++;
	}
	//std::cout << "Zyje3" << std::endl;
	return is_collision;

}

bool collisionBBoxBBox(BBox & box, BBox & box2)
{
	Vector v, v2;
	OldPoint p;
	v.bx = box2.x1;
	v.by = box2.y1;
	v.x = box2.x2 - box2.x1;
	v.y = box2.y2 - box2.y1;
	v2.bx = box2.x1;
	v2.by = box2.y1;
	v2.x = box2.x2 - box2.x1;
	v2.y = box2.y2 - box2.y1;

	p.x = box.x1;
	p.y = box.y1;
	if (projection(v, p) >= box2.height)
		return false;
	if (projection(v2, p) >= box2.width)
		return false;
	p.y = box.y2;
	if (projection(v, p) <= 0)
		return false;
	p.x = box.x2;
	p.y = box.y1;
	if (projection(v2, p) <= 0)
		return false;

	return true;
}

bool collisionBBoxCircle(BBox & box, Circle & circle)
{
	BBox box2;
	Vector v2;
	box2.x1 = circle.o.x - circle.r;
	box2.y1 = circle.o.y - circle.r;
	box2.x2 = circle.o.x + circle.r;
	box2.y2 = circle.o.y + circle.r;

	if (collisionBBoxBBox(box, box2))
		std::cout << "Haha! Mozliwa kolizja z kolkiem!" << std::endl;
	if (collisionBBoxBBox(box, box2))
		return false; // shouldn't it be if(!collisionBBoxBBox(box, &box2)) ?

	// if we reach this point of code it means we had just passed the condition of checking circle specific collision
	OldPoint closest_vertex, vertex_with_farthest_projection;
	if (circle.o.x < box.x1)
	{
		closest_vertex.x = box.x1;
		vertex_with_farthest_projection.x = box.x2;
	}
	else
	{
		closest_vertex.x = box.x2;
		vertex_with_farthest_projection.x = box.x1;
	}

	if (circle.o.y < box.y1)
	{
		closest_vertex.y = box.y1;
		vertex_with_farthest_projection.x = box.y2;
	}
	else
	{
		closest_vertex.y = box.y2;
		vertex_with_farthest_projection.x = box.y1;
	}

	Vector temp_axis(closest_vertex, circle.o), temp_axis2(circle.o,
			closest_vertex);

	if (fabs(
			fabs(projection(temp_axis, vertex_with_farthest_projection))
					- fabs(
							projection(temp_axis2,
									vertex_with_farthest_projection)))
			< circle.r)
		return true;

	return false;
}

bool collisionCircleCircle(Circle & circle1, Circle & circle2)
{
	if (_lenghtPoint(circle1.o, circle2.o) < circle1.r + circle2.r)
		return true;
	else
		return false;
}

bool collisionLineLine(MaskLine &l1, MaskLine &l2)
{
	float a1, a2, b1, b2;
	a1 = l1.getLine().a;
	b1 = l1.getLine().b;
	a2 = l2.getLine().a;
	b2 = l2.getLine().b;
	if(a1 == a2)
	{
		return false;
	}
	OldPoint common = l1.getCommonPoint(l2);
	return l1.checkPoint(common) && l2.checkPoint(common);
}

void draw_from_Mask(MapMask *map) //For debugging purpose
{
	ALLEGRO_COLOR color;
	color = al_map_rgba(0, 0, 255, 255);
	int w = map->size_w, h = map->size_h;

	//std::cout << w << " " << h << std::endl;

	for (int i = 0; i < h; i++)
	{
		//std::cout << i << "!" << std::endl;

		for (int j = 0; j < w; j++)
		{
			//std::cout << j << "?" << std::endl;

			if (map->Mask[i][j] == 1)
				al_draw_pixel(j, i + 1, color);
		}
	}
}

float projection(Vector v, OldPoint p) // function project point p on the vector and return length between beginning of vector and projected point
{ // returned value could be less than 0 !!! (what mean where the point is)
	float cos_alfa;
	OldPoint a;
	a = v.getBeginingPoint();
	OldPoint b;
	b = v.getEndingPoint();
	cos_alfa = (_lenghtPoint(a, p) * _lenghtPoint(a, p) + v.lenght * v.lenght
			- _lenghtPoint(p, b)) / (2 * _lenghtPoint(a, p) * v.lenght);
	return _lenghtPoint(a, p) * cos_alfa;
}

