#ifndef COLLISION_H
#define COLLISION_H

#include "public.h"

class OldPoint
{
public:
	float x, y;
	OldPoint()
	{
		this->x = 0;
		this->y = 0;
	}
	OldPoint(float x, float y);
	OldPoint& operator+(OldPoint &p)
	{
		static OldPoint new_point = OldPoint(x + p.x, y + p.y);
		return new_point;
	}
	OldPoint& operator-(OldPoint &p)
	{
		static OldPoint new_point = OldPoint(x - p.x, y - p.y);
		return new_point;
	}

	OldPoint& operator=(OldPoint &p)
	{
		this->x = p.x;
		this->y = p.y;
		return *this;
	}
};

class Vector
{
public:
	float x, y, lenght;
	float bx, by; //Beginning coords.
	float i, j; //Versors.

	Vector();
	Vector(float x, float y);
	Vector(OldPoint& first, OldPoint& latter, int flag); //Vector from points. Flag: standard, or normal one.
	Vector(Vector& a, Vector &b); //Vector a projected on b
	OldPoint& getBeginingPoint();
	OldPoint& getEndingPoint();

	void draw();
};

struct MapMask
{
	int** Mask;
	int size_w, size_h;
};

class MaskLine;

class BBox2 //Should replace BBox structure in the future.
{
public:
	OldPoint p[5]; //Highest left -> lowest right, and center
	bool is_rotated;
	BBox2()
	{
		is_rotated = false;
		//for (int i = 0; i < 5; i++)
		//	p[i] = Point();
		//getrox: For what?
	}
	bool collisionRotatedBBox(BBox2& box);
	BBox2(OldPoint first, OldPoint second); //Builds a bouding box using two points, stores 4.
	BBox2& operator=(BBox2 &b);
	void draw();
	BBox2& rotate(float delta);
	bool isCollision(MaskLine& line);
	bool isCollision(BBox2 & box);
	float getWidth();
	float getHeight();
	/*
	 * Checks whether the point is in the box.
	 * (´･ω･`) 
	 */
	bool isPointInBox(OldPoint& pt);
};
/**
 * Structure for mathematical representation of the line.
 */
struct Line
{
	float a;
	float b;
};

class Triangle
{
public:
	OldPoint p[3];
	/*
	 * Any order is allowed.
	 */
	Triangle(OldPoint first, OldPoint second, OldPoint third);
	bool isCollision(BBox2 & box);
};

class MaskLine //For collision lines
{
public:
	OldPoint p1, p2; //Points
	Vector normal; //Left normal
	BBox2 box; //Bounding box.
	MaskLine()
	{
	}
	;
	MaskLine(OldPoint first, OldPoint latter);
	void draw();
	Line& getLine();
	Line& getPrependicularLine(OldPoint& pt);
	bool checkPoint(OldPoint & pt);
	OldPoint& getCommonPoint(MaskLine& line);
	/**
	 * Returns the closest point in the line to the given point.
	 */
	OldPoint& getClosestPoint(OldPoint& pt);
};
struct Edges
{
	MaskLine v1[4];
};
Edges& getEdges(BBox2 & box);
struct BBox
{
	float x1, y1, x2, y2;
	int width, height; //Might be useless.
};

inline BBox copyBBox(BBox & box)
{
	BBox new_box;
	new_box.height = box.height;
	new_box.width = box.width;
	new_box.x1 = box.x1;
	new_box.x2 = box.x2;
	new_box.y1 = box.y1;
	new_box.y2 = box.y2;

	return new_box;
}

inline BBox& moveBBox(BBox &box, int x, int y)
{
	static BBox new_box;
	new_box.height = box.height;
	new_box.width = box.width;
	new_box.x1 = box.x1 + x;
	new_box.x2 = box.x2 + x;
	new_box.y1 = box.y1 + y;
	new_box.y2 = box.y2 + y;
	return new_box;
}

class Circle
{
public:
	OldPoint o;
	float r;
	void draw();
};

inline float _lenghtPoint(OldPoint p1, OldPoint p2)
{
	return (sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)));
}

MapMask loadBitmap(ALLEGRO_BITMAP* Bitmap);
void move(BBox2 & box, float x, float y);
void c_showBitmap(MapMask *map);
bool collisionBBoxBBox2(BBox2 &, BBox2 &);
bool collisionRBoxRBox(BBox2 &, BBox2 &);
bool collisionBBox2MaskLine(BBox2 &, MaskLine &);
bool collisionBBoxMask(BBox &, MapMask*);
bool collisionBBoxMask_w_cords(BBox &, MapMask *, int max_y, int max_x,
		int min_y, int min_x);
bool collisionBBoxCircle(BBox & box, Circle & circle);
bool collisionBBoxBBox(BBox & box, BBox & box2);
bool collisionCircleCircle(Circle & circle1, Circle & circle2);
bool collisionLineLine(MaskLine&, MaskLine&);
void draw_from_Mask(MapMask * map);
float projection(Vector v, OldPoint _p);

#endif

