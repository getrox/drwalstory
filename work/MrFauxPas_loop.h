#ifndef MRFAUXPAS_LOOP_H
#define MRFAUXPAS_LOOP_H

#include "public.h"
#include "polygon.h"

int mateuchGeometry(ALLEGRO_DISPLAY* display); //Mutlipoint collision

class DynamicPoint
{
public:
	float x, y;
	float lenght;

	void reset_lenght(const DynamicPoint & which);

};

class DynamicPolygon
{
private:
	float x_min, x_max, y_min, y_max;
	std::vector<DynamicPoint> points;

	std::vector<float> get_intersect_area(const DynamicPolygon & other)
	{
		float max_x, min_x, max_y, min_y;
		std::vector<float> returner;
		//Checks if polygon bounging boxes overlap.
		float lenght1, lenght2;
		lenght1 = abs(x_max - x_min) + abs(other.x_max - other.x_min);
		lenght2 = abs(y_max - y_min) + abs(other.y_max - other.y_min);

		float lenght_between_middles[2];
		lenght_between_middles[0] = (other.x_max + other.x_min) / 2
				- (x_max + x_min) / 2;
		lenght_between_middles[1] = (other.y_max + other.y_min) / 2
				- (y_max + y_min) / 2;

		float distance[2];
		distance[0] = abs(lenght_between_middles[0]) - lenght1;
		distance[1] = abs(lenght_between_middles[1]) - lenght2;

		if (distance[0] > 0 || distance[1] > 0)
			return returner;	//If there's no intersection - return with nothing.

		if (lenght_between_middles[0] > 0)	//Get the minimal coords.
			min_x = other.x_min;
		else
			min_x = x_min;

		if (lenght_between_middles[1] > 0)
			min_y = other.y_min;
		else
			min_y = y_min;
		//And calculate maxes.
		max_x = min_x - distance[0];
		max_y = min_y - distance[1];

		returner.push_back(min_x);
		returner.push_back(max_x);
		returner.push_back(min_y);
		returner.push_back(max_y);

		return returner;
	}
	std::vector<int> get_intersect_points(const DynamicPolygon & other)
	{
		std::vector<int> which;

		std::vector<float> area = get_intersect_area(other);
		if(area.size() == 0)
			return which;
		int size = points.size();
		//Put points in the area into this.

		for(int i = 0; i < size; i++)
			if (points[i].x > area[0] && points[i].x < area[1])
				if(points[i].y > area[2] && points[i].y < area[3])
					which.push_back(i);
		which.push_back(-1);
		size = other.points.size();

		for(int i = 0; i < size; i++)
			if (other.points[i].x > area[0] && other.points[i].x < area[1])
				if(other.points[i].y > area[2] && other.points[i].y < area[3])
					which.push_back(i);

		return which;
	}

public:
	DynamicPolygon(const std::vector<DynamicPoint> & lists);

	void add_front(const DynamicPoint & p);
	void add_end(const DynamicPoint & p);

	void insert(const DynamicPoint &p, const int & prev);
	void remove(const int &p);

	void move(const float &x, const float &y);
	void move_point(const int & p, const float & x, const float & y);

	void rotate(const float & degree);

	bool collision(const DynamicPolygon & other);
};

#endif
