/*
 * triangle.h
 *
 *  Created on: 2012-04-29
 *      Author: getrox
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_
#include "public.h"
#include <boost/geometry/geometries/polygon.hpp>

typedef boost::geometry::model::polygon<Point> BoostTriangle;

class Triangle
{
private:
	float x1,y1,x2,y2;
public:
	Triangle();
	Triangle(float, float, float, float);
	BoostTriangle geometry;
};

#endif /* TRIANGLE_H_ */
