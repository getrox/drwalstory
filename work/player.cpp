#include "public.h"
#include "player.h"
#include "collision.h"

Player::Player() //General player init.
{
	s_x = 0; //Velocity stuff
	s_y = 0;
	jump = false; //Flag for jumping

	pos_x = 50; //Starting pos
	pos_y = 50;
	// Le image
	drwal_image = al_load_bitmap("assets/drwal.png");

	if (!drwal_image)
		std::cout << "Couldn't load yer axeman. Ye got yer files right ?"
				<< drwal_image << std::endl;

	//setting our bounding box.

	drwal_box.width = al_get_bitmap_width(drwal_image) - 14;
	drwal_box.height = al_get_bitmap_height(drwal_image) - 6;
	drwal_box.x1 = pos_x + 7;
	drwal_box.y1 = pos_y + 3;
	drwal_box.x2 = pos_x + 7 + drwal_box.width;
	drwal_box.y2 = pos_y + 3 + drwal_box.height;

}

void Player::sterowanie(bool *ster, MapMask *map) //My second attempt. Tried to get the farthest pixel coords
{ //from the mask, which we use to move our character.
	BBox tmp; //Less calculations, but probably I've messed up the function
	float x = 0, y = 0; //which does that, so there are a few bugs.
	int max_x = 0, min_x = 0, max_y = 0, min_y = 0;
	tmp = copyBBox(drwal_box);
	bool can_move = true;

	if (ster[0])
		y -= 5;

	if (ster[1])
		y += 5;

	if (ster[2])
		x += 5;

	if (ster[3])
		x -= 5;

	can_move = !collisionBBoxMask_w_cords(moveBBox(tmp, x, y), map, max_y,
			max_x, min_y, min_x);

	if (!can_move)
	{
		//std::cout << "min_x: " << min_x << " x2: " << drwal_box.x2 << "\n"
		//          << "max_x: " << max_x << " x1: " << drwal_box.x1 << "\n"
		//          << "max_y: " << max_y << " y2: " << drwal_box.y2 << "\n"
		//          << "min_y: " << min_y << " y1: " << drwal_box.y1 << "\n"
		//          << std::endl;

		if (x > 0)
			x = min_x - drwal_box.x2 - 1;
		else if (x < 0)
			x = max_x - drwal_box.x1 + 1;

		if (y > 0)
			y = min_y - drwal_box.y2 - 1;
		else if (y < 0)
			y = max_y - drwal_box.y1 + 1;

		//std::cout << "x: " << x << " y: " << y << std::endl;
		if (x != 0 || y != 0)
			can_move = !collisionBBoxMask(moveBBox(tmp, x, y), map);
	}
	if (can_move)
	{
		pos_x += x;
		pos_y += y;
		drwal_box = moveBBox(drwal_box, x, y);
	}

}

void Player::sterowanie2(bool *ster, MapMask *map) //Generally tries to first collect where you'd like to move.
{ //And then check pixel by pixel if that's possible. No real difference
	BBox tmp; //between x an y axis. Works, but quite buggy.
	float x = 0, y = 0;
	tmp = copyBBox(drwal_box);

	bool can_move = true;
	if (ster[0])
		y -= 10;

	if (ster[1])
		y += 10;

	if (ster[2])
		x += 10;

	if (ster[3])
		x -= 10;

	can_move = !collisionBBoxMask(moveBBox(tmp, x, y), map);

	if (!can_move)
	{

		if (y > 0)
		{
			for (int i = 1; i < y && !can_move; i++)
			{
				can_move = !collisionBBoxMask(moveBBox(tmp, x, i), map);
				if (can_move)
					y = i;
			}
		}
		else if (y < 0)
		{
			for (int i = -1; i > y; i--)
			{
				can_move = !collisionBBoxMask(moveBBox(tmp, x, i), map);
				if (can_move)
					y = i;
			}
		}

		if (x > 0)
		{
			for (int j = 1; j < x; j++)
			{
				can_move = !collisionBBoxMask(moveBBox(tmp, j, y), map);
				if (can_move)
					x = j;
			}

		}
		else if (x < 0)
		{
			for (int j = -1; j > x; j++)
			{
				can_move = !collisionBBoxMask(moveBBox(tmp, j, y), map);
				if (can_move)
					x = j;
			}
		}

	}
	if (can_move)
	{
		pos_x += x;
		pos_y += y;
		drwal_box = moveBBox(drwal_box, x, y);
	}

}

void Player::sterowanie3(MapMask *map) //Safe, but not optimized. Tho the character can get stuck.
{
	BBox tmp; //Temporary box, for checking where can we go.
	int sol_x[8] =
	{ 1, 1, 1, 1, 1, 1, 1, 2 }, //Solution for movement -> where can the player go, 2 pixels east and 1 or 7 up?
			sol_y[8] =
			{ 0, -1, -2, -3, -4, -5, -6, -7 };
	int solution = 0, i = 1; //For indexing the solution stuff, see for loops.
	int max_x = abs(s_x), max_y = abs(s_y); //The max movement, considering current velocity.
	//jump = false;

	tmp = copyBBox(drwal_box);
	bool can_move = false;

	if (s_y < 0) //Move up( jump)
	{
		i = 1;
		while (i <= max_y) //Checking all the pixels <= max velocity for y
		{
			tmp = copyBBox(drwal_box);
			can_move = !collisionBBoxMask(moveBBox(tmp, 0, -1), map); //Can we go 1 pixel up ?
			if (can_move) //Yes, we can! Go 1 pixel up!
			{

				i++;
				pos_y -= 1;
				drwal_box = moveBBox(drwal_box, 0, -1);
				//std::cout << pos_y << std::endl;
			}
			else //No, you can't. No point in checking more.
			{
				s_y = s_y / 2;
				break;
			}
		}
	}

	else if (s_y > 0) //Move down. Rest is analogical for move up stuff.
	{
		i = 1;
		while (i <= max_y)
		{
			can_move = false;
			tmp = copyBBox(drwal_box);
			can_move = !collisionBBoxMask(moveBBox(tmp, 0, 1), map);
			if (can_move)
			{
				i++;
				pos_y += 1;
				drwal_box = moveBBox(drwal_box, 0, 1);
			}
			else
			{
				s_y = 0;
				break;
			}
		}
	}
	if (s_x > 0) //Move left.
	{
		i = 1;
		while (i <= max_x) //To check all the pixels before me
		{
			can_move = false;
			tmp = copyBBox(drwal_box); //reseting for each movement.
			for (solution = 0; !can_move && solution <= 7; 1) //Checking all solutions
			{
				can_move = !collisionBBoxMask(moveBBox(tmp, sol_x[solution],
						sol_y[solution]), map); //Can i move with solution nr X ?
				if (!can_move)
					solution += 1;
			}
			if (can_move) //Yes I can!
			{
				if (solution == 7) //If using solution nr 7, you go two pixels further.
					i += 2;
				else
					//If not, you just go 1 pixel.
					i += 1;

				pos_x += sol_x[solution]; //Moove!
				pos_y += sol_y[solution];
				drwal_box = moveBBox(drwal_box, sol_x[solution],
						sol_y[solution]);
			}
			else
				break;
		}
	}

	else if (s_x < 0) //Move left. Analogical. We use "-" before sol'ution'_x to move left.
	{
		i = 1;
		while (i <= max_x)
		{
			can_move = false;
			tmp = copyBBox(drwal_box);
			for (solution = 0; !can_move && solution <= 7; 1)
			{

				can_move = !collisionBBoxMask(moveBBox(tmp, -sol_x[solution],
						sol_y[solution]), map);
				if (!can_move)
					solution += 1;
			}
			if (can_move)
			{
				if (solution == 7)
					i += 2;
				else
					i += 1;

				pos_x += -sol_x[solution];
				pos_y += sol_y[solution];
				drwal_box = moveBBox(drwal_box, -sol_x[solution],
						sol_y[solution]);
			}
			else
				break;
		}
	}

	if (collisionBBoxMask(moveBBox(tmp, 0, 5), map))
		jump = true;
	else
		jump = false;

}

void Player::debug_ster(bool *ster, MapMask *map) //For debugging purposes. We move everywhere. Like a ghost.
{

	float x = 0, y = 0;
	bool can_move = true;

	if (ster[0])
		s_y -= 0.25;

	else if (ster[1])
		s_y += 0.25;

	else
		s_y /= 1.5;

	if (ster[2])
		s_x += 0.25;

	else if (ster[3])
		s_x -= 0.25;

	else
		s_x /= 1.5;

	x = s_x;
	y = s_y;

	pos_x += x;
	pos_y += y;
	drwal_box = moveBBox(drwal_box, x, y);

	if (collisionBBoxMask(drwal_box, map))
		std::cout << "Kolizja! " << drwal_box.x1 << " " << drwal_box.x2 << " "
				<< drwal_box.y1 << " " << drwal_box.y2 << "\n" << std::endl;

}

void Player::acceleration(bool *ster) //When you'd like to mess up with velocity... try this.
{
	if (!jump)
	{
		s_y += 0.5;
		/*if (s_y > 12.5)
		 s_y = 12.5;*/
	}
	else
	{
		if (ster[0])
			s_y = -8;

		if (ster[2])
		{
			s_x += 0.25;
			if (s_x > 5)
				s_x = 5;
		}
		else if (ster[3])
		{
			s_x -= 0.25;
			if (s_x < -5)
				s_x = -5;
		}
		else
			s_x /= 1.5;
	}
}
