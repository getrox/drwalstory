/*
 * box.h
 *
 *  Created on: 2012-04-29
 *      Author: getrox
 */

#ifndef BOX_H_
#define BOX_H_

#include <boost/geometry/geometries/box.hpp>

typedef boost::geometry::model::box BoostBox;

class Box
{
private:
	float x1,y1,x2,y2;
public:
	Box();
	Box(float, float, float, float);
	BoostBox geometry;
};
#endif /* BOX_H_ */
