#include "public.h"
#include "collision.h"
#include "player.h"
#include "addons.h"

#include "MrFauxPas_loop.h"

int mateuchGeometry(ALLEGRO_DISPLAY* display)
{
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_MOUSE_CURSOR *cursor[5] =
	{ NULL, NULL, NULL, NULL, NULL };
	ALLEGRO_BITMAP *cursor_bmp[5] =
	{ NULL, NULL, NULL, NULL, NULL };

	ALLEGRO_BITMAP *map = NULL;
	ALLEGRO_COLOR color = al_map_rgba(255, 0, 0, 100);

	//Vectors of pointers to Polygons, which serve as obstacles.
	std::vector<Polygon*> obstacles;
	std::vector<Polygon*> living_things;

	//Points that tell you which object, and which point are selected.
	std::deque<int> selected_obstacles;

	int which_obstacle = -1;
	int which_point = -1;

	bool movement_control[9] = //For character controling
			{ false, false, false, false, false, false, false, false, false };

	std::vector<Point> multipoint;
	for (int i = 0; i < 10; i++)
	{
		Point point;
		point.x(i * i);
		point.y(i * i * i / 4);

		multipoint.push_back(point);
	}
	multipoint.push_back(multipoint[0]);
	Polygon multiman(multipoint);

	multipoint.clear();
//	{
//		Point point(230, 40);
//		multipoint.push_back(point);
//
//		Point point2(280, 70);
//		multipoint.push_back(point2);
//
//		Point point3(330, 170);
//		multipoint.push_back(point3);
//		multipoint.push_back(point);
//	}
//
//	Polygon other(multipoint);
//	obstacles.push_back(&other);
	event_queue = al_create_event_queue();
	if (!event_queue)
		return -1;

	timer = al_create_timer(1.0 / FPS);
	if (!timer)
		return -2;

	map = al_create_bitmap(800, 640);
	if (!map)
		return -3;
	al_set_target_bitmap(map);
	al_clear_to_color(al_map_rgb(104, 20, 0));
	al_set_target_bitmap(al_get_backbuffer(display));

	//Getting cursors.
	cursor_bmp[0] = al_load_bitmap("assets/cursor.png");
	cursor[0] = al_create_mouse_cursor(cursor_bmp[0], 3, 11);

	cursor_bmp[1] = al_load_bitmap("assets/kolko1.png");
	cursor[1] = al_create_mouse_cursor(cursor_bmp[1], 42, 20);

	cursor_bmp[2] = al_load_bitmap("assets/kolko2.png");
	cursor[2] = al_create_mouse_cursor(cursor_bmp[2], 24, 18);

	cursor_bmp[3] = al_load_bitmap("assets/hammer.png");
	cursor[3] = al_create_mouse_cursor(cursor_bmp[3], 2, 10);

	cursor_bmp[4] = al_load_bitmap("assets/knife.png");
	cursor[4] = al_create_mouse_cursor(cursor_bmp[4], 0, 0);
	al_set_mouse_cursor(display, cursor[0]);

	for (int i = 0; i < 5; i++)
	{
		if (!cursor_bmp[i])
			return -4;

		if (!cursor[i])
			return -5;
	}

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_mouse_event_source());

	bool redraw = false;

	al_start_timer(timer);
	multiman.move(0, 0);
	//Variables for mouse moving.
	int move_point_x = 0, move_point_y = 0;
	int mouse_point_x = 0, mouse_point_y = 0;

	int mouse_mode = 0;

	bool mouse_button_1_pressed = false, mouse_button_2_pressed = false;
	while (1) //Main loop. I'll put it in a seperate function later.
	{
		ALLEGRO_EVENT ev;

		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) //When it's the 1/60 of the second.
		{

			switch (mouse_mode)
			{
			case 0:
				al_set_mouse_cursor(display, cursor[0]);
				break;
			case 1:
				if (which_obstacle == -1)
					al_set_system_mouse_cursor(display,
							ALLEGRO_SYSTEM_MOUSE_CURSOR_QUESTION);
				else if (!obstacles[which_obstacle]->closed)
					al_set_mouse_cursor(display, cursor[1]);
				else
					al_set_mouse_cursor(display, cursor[2]);
				break;
			case 2:
				al_set_mouse_cursor(display, cursor[3]);
				break;
			case 3:
				al_set_mouse_cursor(display, cursor[4]);
				break;
			case 4:
				al_set_system_mouse_cursor(display,
						ALLEGRO_SYSTEM_MOUSE_CURSOR_MOVE);
				break;
			case 5:
				al_set_system_mouse_cursor(display,
						ALLEGRO_SYSTEM_MOUSE_CURSOR_ALT_SELECT);
				break;
			}

			redraw = true;

			//Collision of polygon vs polygon.
//			Polygon otherTester(other);
			float x = 0, y = 0;
			float rot = 0;
			if (movement_control[6])
				break;
//			if (movement_control[5]) //Changing mouse mode.
//				mouse_mode = (mouse_mode + 1) % 4;
//			if (movement_control[4])
//				mouse_mode = abs((mouse_mode - 1) % 4);
			if (movement_control[0]) //Controling our test box named pudlo1.
				y = -1;
			if (movement_control[1])
				y = 1;
			if (movement_control[2])
				x = 1;
			if (movement_control[3])
				x = -1;
			if (movement_control[7])
				rot = 1;
			if (movement_control[8])
				rot = -1;

			multiman.moveIfCan(x, y, rot, obstacles);

			if (!mouse_button_1_pressed)
				which_obstacle = -1;
			for (int i = 0; i < obstacles.size() && which_obstacle == -1; i++)
				if (obstacles[i]->mouseCollision(mouse_point_x, mouse_point_y))
					which_obstacle = i;

			//Moving the other's (polygon) dots.
			if (which_obstacle != -1)
			{
				if (!mouse_button_1_pressed) //If button 1 is up (not pressed), reset collision point.
					which_point = -1;

				//Searches for the one to collide with. If which_points is  -1, then skip this.
				if (which_point == -1)
					which_point =
							obstacles[which_obstacle]->mousePointCollision(
									mouse_point_x, mouse_point_y);
			}
			switch (mouse_mode)
			{
			case 0:
			{
				if (mouse_button_1_pressed)
				{
					if (which_obstacle != -1)
						obstacles[which_obstacle]->movePointIfCan(mouse_point_x,
								mouse_point_y, which_point, multiman);
				}
				break;
			}

			case 1:
			{
				//Opening and closing of the polygon
				if (mouse_button_1_pressed)
				{
					if (which_obstacle != -1)
						obstacles[which_obstacle]->changeIfCan(true, NULL,
								false, -1, -1, multiman);
					mouse_button_1_pressed = false;
				}
				break;
			}
			case 2:
			{
				//Adding points - at the end. Or removing them - where you click.
				if (mouse_button_1_pressed)
				{
					Point add_me;
					add_me.x(mouse_point_x);
					add_me.y(mouse_point_y);
					if (selected_obstacles.size() > 0)
						obstacles[selected_obstacles[0]]->changeIfCan(false,
								&add_me, true, -1, -1, multiman);
					else
					{
						Polygon *created;
						created = new Polygon;
						created->geometry.push_back(Point(add_me));
						obstacles.push_back(created);
						selected_obstacles.push_front(obstacles.size() - 1);
					}
					mouse_button_1_pressed = false;
				}
				else if (mouse_button_2_pressed)
				{
					if (which_obstacle != -1)
					{
						obstacles[which_obstacle]->changeIfCan(false, NULL,
								false, which_point, -1, multiman);
						if (obstacles[which_obstacle]->geometry.size() == 0)
						{
							std::vector<Polygon*>::iterator it =
									obstacles.begin();
							delete obstacles[which_obstacle];
							obstacles.erase(it + which_obstacle);

							std::deque<int>::iterator jit = selected_obstacles.begin();
							int size = selected_obstacles.size();
							for(int i = 0; i < size; i++)
								if(which_obstacle == selected_obstacles[i])
								{
									selected_obstacles.erase(jit+i);
								}
						}
					}
					mouse_button_1_pressed = false;
				}
				break;
			}
			case 3:
			{
				//Inserting points between two points.
				if (mouse_button_1_pressed)
				{
					if (which_obstacle != -1)
						obstacles[which_obstacle]->changeIfCan(false, NULL,
								false, -1, which_point, multiman);
					mouse_button_1_pressed = false;
				}
				//Dividing polygons by removing one point.
				else if(mouse_button_2_pressed)
				{
					if(which_obstacle != -1 && which_point != -1)
					{
						Polygon * tmp = obstacles[which_obstacle]->divide(which_point);
						//If there's a new Polygon (adress != 0), add it.
						if (tmp != 0)
							obstacles.push_back(tmp);
						//If the polygon which we divided is empty, remove it!
						if(obstacles[which_obstacle]->geometry.size() == 0)
						{
							std::vector<Polygon*>::iterator it = obstacles.begin();
							obstacles.erase(it + which_obstacle);
						}
							std::cout << obstacles.size() << std::endl;
					}
					mouse_button_1_pressed = false;

				}
				break;
			}
			case 4:
			{
				//Moving whole figure.
				if (mouse_button_1_pressed)
				{
					for (int i = 0; i < selected_obstacles.size(); i++)
					{
//							Point center;
//							boost::geometry::centroid(obstacles[i]->geometry, center);
//							obstacles[i]->moveIfCan(mouse_point_x - center.x(), mouse_point_y, 0, multiman);
						obstacles[selected_obstacles[i]]->moveAbs(mouse_point_x,
								mouse_point_y);
					}
				}
				break;
			}
			case 5:
			{
				//Selecting polygons.
				if (mouse_button_1_pressed)
					if (which_obstacle != -1)
					{
						//Could implement BST here.
						bool is_there = false;

						int size = selected_obstacles.size();
						for (int i = 0; i < size; i++)
							std::cout << selected_obstacles[i] << " ";
						std::cout << std::endl;
						if (size > 0 && which_obstacle == selected_obstacles[0])
						{
							selected_obstacles.pop_front();
						}
						else
						{
							for (int i = 1; i < size && !is_there; i++)
								if (selected_obstacles[i] == which_obstacle)
								{
									is_there = true;
									int tmp = selected_obstacles[i];
									selected_obstacles[i] =
											selected_obstacles[0];
									selected_obstacles[0] = tmp;
								}
							if (!is_there)
								selected_obstacles.push_back(which_obstacle);
						}
						mouse_button_1_pressed = false;
					}
				break;
			}

			}
		}

		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
				movement_control[0] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
				movement_control[1] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
				movement_control[2] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
				movement_control[3] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_A)
			//movement_control[4] = true;
			{
				mouse_mode = mouse_mode - 1 % 6;
				if (mouse_mode == -1)
					mouse_mode = 5;
			}
			if (ev.keyboard.keycode == ALLEGRO_KEY_S)
				//movement_control[5] = true;
				mouse_mode = (mouse_mode + 1) % 6;
			if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				movement_control[6] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_Q)
				movement_control[7] = true;
			if (ev.keyboard.keycode == ALLEGRO_KEY_W)
				movement_control[8] = true;
		}

		else if (ev.type == ALLEGRO_EVENT_KEY_UP)
		{
			if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
				movement_control[0] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
				movement_control[1] = false;

			if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
				movement_control[2] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
				movement_control[3] = false;

			if (ev.keyboard.keycode == ALLEGRO_KEY_A)
				movement_control[4] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_S)
				movement_control[5] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				movement_control[6] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_Q)
				movement_control[7] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_W)
				movement_control[8] = false;
		}

		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			break;

		if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (ev.mouse.button == 1)
				mouse_button_1_pressed = true;
			if (ev.mouse.button == 2)
				mouse_button_2_pressed = true;
		}

		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
		{
			if (ev.mouse.button == 1)
				mouse_button_1_pressed = false;
			if (ev.mouse.button == 2)
				mouse_button_2_pressed = false;
		}

		if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			//Get mouse position.
			mouse_point_x = ev.mouse.x;
			mouse_point_y = ev.mouse.y;
			move_point_x = ev.mouse.dx;
			move_point_y = ev.mouse.dy;
		}
		else
		{
			//If there's no mouse movement - no movement for move_point.
			move_point_x = 0;
			move_point_y = 0;
		}

		if (redraw && al_is_event_queue_empty(event_queue))
		{
			redraw = false;
			al_clear_to_color(al_map_rgb(0, 0, 0));
			al_draw_bitmap(map, 0, 0, 0);
			multiman.draw();

			for (int i = 0; i < obstacles.size(); i++)
				obstacles[i]->draw();

			if (which_obstacle != -1)
			{
				obstacles[which_obstacle]->draw(al_map_rgba(150, 100, 30, 100));
				if (which_point != -1)
					obstacles[which_obstacle]->drawPoint(which_point);

			}
			if (selected_obstacles.size() > 0)
			{
				obstacles[selected_obstacles[0]]->draw(
						al_map_rgba(200, 60, 100, 100));
				for (int i = 1; i < selected_obstacles.size(); i++)
					obstacles[selected_obstacles[i]]->draw(
							al_map_rgba(100, 30, 255, 100));
			}
			al_flip_display();
		}
	}
	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_bitmap(map);
	for (int i = 0; i < 5; i++)
	{
		al_destroy_mouse_cursor(cursor[i]);
		al_destroy_bitmap(cursor_bmp[i]);
	}
	return 0;
}

