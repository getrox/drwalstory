/*
 * polygon.h
 *
 *  Created on: 2012-04-29
 *      Author: getrox
 */

#ifndef POLYGON_H_
#define POLYGON_H_

#include "public.h"
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry.hpp>
#include <boost/foreach.hpp>

typedef boost::geometry::model::linestring<Point, std::deque> BoostPolygon;

//!Polygon class.
class Polygon
{
public:

	//!Boost's geometry's linestring.
	BoostPolygon geometry;
	//!Closed parameter.
	bool closed;

	//!Default constructor for Polygon.
	/*!
	 Creates a closed Polygon.
	 */
	Polygon();
	//!Constructor.
	/*!
	 If end point == beginning point, we get a closed circle.
	 \param tab Vector of points.
	 */
	Polygon(const std::vector<Point> &tab);

	//!Copy constructor.
	/*!
	 \param old Polygon we'd like to copy.
	 */
	Polygon(const Polygon & old);

	//!Overloaded assignment operator.
	/*!Copies a Polygon.
	 \param old Polygon we'd like to copy.
	 \return A new Polygon, which is a copy of old.
	 */
	Polygon operator=(const Polygon & old);

	//!Draws Polygon.
	/*!
	 \param color Color used to draw.
	 */
	void draw(const ALLEGRO_COLOR &color = al_map_rgba(30, 200, 27, 100));
	//!Draws a Point from Polygon.
	/*!
	 \param p Chosen point from the geometry's std::deque.
	 \param color Color used to draw.
	 */
	void drawPoint(const int &p,
			const ALLEGRO_COLOR &color = al_map_rgba(130, 10, 10, 100));

	//!Moves whole Polygon from it's position.
	/*!
	 \param x Move X horizontally. -Left Right+
	 \param y Move Y vertically. -Up Down+
	 */
	void move(float const x, float const y);

	//!Sets a new position for Polygon.
	/*!Sets a new position relatively to Polygon 's mass center.
	 \param x New X position.
	 \param y New Y position.
	 */
	void moveAbs(float const x, float const y);
	//!Moves one Point from Polygon.
	/*!
	 \param p Chosen point from the geometry's std::deque.
	 \param x Move X horizontally. -Left Right+
	 \param y Move Y vertically. -Up Down+
	 */
	void movePoint(const int &p, const float &x, const float &y);
	//!Sets new position for one Point from Polygon.
	/*!Sets a new position relatively to Polygon 's mass center.
	 \param p Chosen Point from the geometry's std::deque.
	 \param x New X position.
	 \param y New Y position.
	 */
	void movePointAbs(const int &p, const float &x, const float &y);

	//!Removes Point from Polygon 's std::deque
	/*!
	 \param which Chosen Point from geometry's std::deque.
	 */
	void removePoint(const int &which);

	//!Rotates the whole Polygon
	/*!
	 \param rot Angle, in degrees.
	 */
	void rotate(float const &rot);

	//!Closes the Polygon. Removes end == begin Point, sets closed to false.
	void close();
	//!Opens the Polygon. Adds end == begin Point, sets closed to true;
	void open();

	//!Adds Point to the Polygon , at the beginning or end.
	/*!
	 \param p Point added.
	 \param end If true, added at Polygon's end, else and the beginning.
	 */
	void addPoint(const Point &p, const bool &end = true);
	//!Divides (by creating new Point) a line before Point _next.
	/*!
	 \param next Chosen Point from the geometry's std::deque.
	 */
	void insertPoint(const int &next);

	//!Checks if Point x,y is within radius from Polygon.
	/*!
	 \param x X-position
	 \param y Y-position
	 \param radius Radius.
	 \return If 'x', 'y' is within radius from any edge of the Polygon, return true.
	 */


	//!Divides the Polygon, by removing the Point nr p.
	/*!
	 \param p Point's number to be removed.
	 \return Pointer to a new Polygon.
	 */
	Polygon* divide(const int &p);

	bool mouseCollision(const float &x, const float &y, const float &radius =
			5.0);
	//!Checks if Point x, y is within radius from any Point belonging to Polygon.
	/*!
	 \param x X-positon
	 \param y Y-position
	 \param radius Radius.
	 \return If 'x', 'y' is within radius from any Point of the Polygon, return that Point 's nr. Else return '-1'
	 */
	int mousePointCollision(const float &x, const float &y,
			const float &radius = 5.0);

	//Polygon &other, int &which - these should be changed to lists, stacks, or whatever - so you could check collision with all
	//of things in these containers.

	//!Checks if two Polygons collide
	/*!
	\param other The other Polygon.
	\return True if Polygons intersect each other.
	 */
	bool isCollision(const Polygon &other);

	//!Move Polygon, if no collision with other Polygon 's detected. Uses various functions for various parameters.
	/*!
	 \param x Uses Polygon::move
	 \param y Uses Polygon::move
	 \param rot Uses Polygon::rotate
	 \param other List of Polygons to check the collision with.
	 */
	void moveIfCan(const float &x, const float &y, const float &rot,
			const std::vector<Polygon*> &other);
	//!Move Polygon 's Point, if no collision with other Polygon 's detected. Uses various functions for various parameters.
	/*!
	 \param x Uses Polygon::movePointAbs
	 \param y Uses Polygon::movePointAbs
	 \param p Chosen Point from the geometry's std::deque
	 \param other Polygon to check the collision with.
	 */
	void movePointIfCan(const float &x, const float &y, const int &p,
			const Polygon &other);
	//!Move Polygon, if no collision with other Polygon 's detected. Uses various functions for various parameters.
	/*!
	 \param change_closed If true, changes the closed/open status with Polygon::close or Polygon::open functions.
	 \param add If not NULL, add's this Point to Polygon. Uses Polygon::addPoint .
	 \param at_end Only used if add != NULL. Polygon::addPoint .
	 \param remove If != -1, removes Point with given number. Uses Polygon::removePoint .
	 \param knife  If != -1, adds a new Point before the Point with given number. Uses Polygon::remove
	 \param other Polygon to check the collision with.
	 */
	void changeIfCan(bool change_closed, Point *add, bool at_end, int remove,
			int knife, const Polygon &other);

};

#endif /* POLYGON_H_ */
