#include "public.h"
#include "collision.h"
#include "player.h"
#include "addons.h"
#include "loop.h"

int collisionPixelLoop(ALLEGRO_DISPLAY* display)
{
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	ALLEGRO_BITMAP *mask_image = NULL; //Mask image, and viewable image
	ALLEGRO_BITMAP *map = NULL;
	ALLEGRO_COLOR color = al_map_rgba(255, 0, 0, 100);
	Circle circ;
	circ.r = 50;
	circ.o.x = 300;
	circ.o.y = 350;

	MapMask mask;
	Player box_player[1];

	bool movement_control[7] = //For character controling
			{ false, false, false, false, false, false, false };
	bool toggle_mask = false;

	event_queue = al_create_event_queue();
	if (!event_queue)
		return -1;

	timer = al_create_timer(1.0 / FPS);
	if (!timer)
		return -2;

	//Images!
	map = al_load_bitmap("assets/map1.png");
	mask_image = al_load_bitmap("assets/collisionmap1.png");
	if (!map || !mask_image)
		return -3;

	mask = loadBitmap(mask_image);

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	bool redraw = false;

	al_start_timer(timer);

	while (1) //Main loop. I'll put it in a seperate function later.
	{
		ALLEGRO_EVENT ev;

		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) //When it's the 1/60 of the second.
		{
			if (movement_control[6])
				break;
			redraw = true;
			for (int i = 0; i < 1; i++)
			{
				//gracz[i].acceleration(ster);
				//gracz[i].sterowanie3(&_Mask);
				box_player[i].debug_ster(movement_control, &mask);
				if (movement_control[5])
				{
					box_player[i].drwal_box = moveBBox(box_player[i].drwal_box,
							50 - box_player[i].pos_x, 50 - box_player[i].pos_y);
					box_player[i].pos_x = 50;
					box_player[i].pos_y = 50;
				}
			}
			if (movement_control[4])
			{
				if (toggle_mask)
					toggle_mask = false;
				else
					toggle_mask = true;
			}

			if (collisionBBoxCircle(box_player[0].drwal_box, circ))
			{
				std::cout << "Lol kolizja x: " << box_player[0].drwal_box.x1
						<< " y: " << box_player[0].drwal_box.y1 << std::endl;
				al_draw_pixel(box_player[0].drwal_box.x1,
						box_player[0].drwal_box.y1, color);
			}

			//gracz[0].debug_ster(ster, &_Mask);

		}

		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) //Hold the key down -> get trues -> move yer character!
		{
			if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
				movement_control[0] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
				movement_control[1] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
				movement_control[2] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
				movement_control[3] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_H)
				movement_control[4] = true;
			if (ev.keyboard.keycode == ALLEGRO_KEY_R)
				movement_control[5] = true;
			if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				movement_control[6] = true;

		}

		else if (ev.type == ALLEGRO_EVENT_KEY_UP) //Leave the key alone -> get fales -> your character is
		{ //from you, you sadist!
			if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
				movement_control[0] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
				movement_control[1] = false;

			if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
				movement_control[2] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
				movement_control[3] = false;

			if (ev.keyboard.keycode == ALLEGRO_KEY_H)
				movement_control[4] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_R)
				movement_control[5] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				movement_control[6] = true;
		}

		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) //Getting out from this infinity loop.
		{
			break;
		}

		if (redraw && al_is_event_queue_empty(event_queue)) //No events, and we need to redraw ? Let's get to work!
		{
			redraw = false; //Not optimalized.
			al_clear_to_color(al_map_rgb(0, 0, 0));
			if (!toggle_mask)
				al_draw_bitmap(map, 0, 0, 0);
			else
				al_draw_bitmap(mask_image, 0, 0, 0);
			//draw_from_Mask(&_Mask);

			//for (int i = 0; i < 1; i++)
			//al_draw_bitmap(gracz[i].drwal_image, gracz[i].pos_x,
			//gracz[i].pos_y, 0);
			//////////DRAW LINES////////// Checking if they go after the bounding box
			al_draw_line(box_player[0].drwal_box.x1 + 1,
					box_player[0].drwal_box.y1, box_player[0].drwal_box.x2 + 1,
					box_player[0].drwal_box.y1, color, 0);
			al_draw_line(box_player[0].drwal_box.x2 + 1,
					box_player[0].drwal_box.y1, box_player[0].drwal_box.x2 + 1,
					box_player[0].drwal_box.y2, color, 0);
			al_draw_line(box_player[0].drwal_box.x1 + 1,
					box_player[0].drwal_box.y1, box_player[0].drwal_box.x1 + 1,
					box_player[0].drwal_box.y2, color, 0);
			al_draw_line(box_player[0].drwal_box.x1 + 1,
					box_player[0].drwal_box.y2, box_player[0].drwal_box.x2 + 1,
					box_player[0].drwal_box.y2, color, 0);
			al_draw_pixel(box_player[0].drwal_box.x1,
					box_player[0].drwal_box.y1, color);
			al_draw_pixel(box_player[0].drwal_box.x1,
					box_player[0].drwal_box.y2, color);
			al_draw_pixel(box_player[0].drwal_box.x2,
					box_player[0].drwal_box.y1, color);
			al_draw_pixel(box_player[0].drwal_box.x2,
					box_player[0].drwal_box.y2, color);

			circ.draw();

			al_flip_display();
		}
	}
	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_bitmap(map);
	al_destroy_bitmap(mask_image);
	return 0;
}

int collisionVectorLoop(ALLEGRO_DISPLAY* display)
{
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	ALLEGRO_BITMAP *map = NULL;
	ALLEGRO_COLOR color = al_map_rgba(255, 0, 0, 100);

	//player gracz[1];
	BBox2 box1(OldPoint(135, 150), OldPoint(165, 200));

	BBox2 test_box = box1; // test for collision
	BBox2 box2(OldPoint(60, 75), OldPoint(125, 140));
	BBox2 box3 = box2.rotate(4);
	MaskLine line(OldPoint(170, 220), OldPoint(240, 500));
	MaskLine edge1(OldPoint(600, 200), OldPoint(600, 300));
	MaskLine edge2(OldPoint(600, 200), OldPoint(300, 300));
	MaskLine edge3(OldPoint(600, 300), OldPoint(300, 300));
	Triangle triangle(OldPoint(300, 300), OldPoint(600, 200),
			OldPoint(600, 300));
	bool movement_control[9] = //For character controling
			{ false, false, false, false, false, false, false, false, false };
	bool toggle_mask = false;
	bool collision = false;

	event_queue = al_create_event_queue();
	if (!event_queue)
		return -1;

	timer = al_create_timer(1.0 / FPS);
	if (!timer)
		return -2;

	map = al_create_bitmap(800, 640);
	if (!map)
		return -3;
	al_set_target_bitmap(map);
	al_clear_to_color(al_map_rgb(30, 122, 45));
	al_set_target_bitmap(al_get_backbuffer(display));

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	bool redraw = false;

	al_start_timer(timer);

	while (1) //Main loop. I'll put it in a seperate function later.
	{
		ALLEGRO_EVENT ev;

		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) //When it's the 1/60 of the second.
		{
			collision = false;
			float x = 0, y = 0;
			redraw = true;
			if (movement_control[6])
				break;
			if (movement_control[0]) //Controling our test box named pudlo1.
				y = -1;
			if (movement_control[1])
				y = 1;
			if (movement_control[2])
				x = 1;
			if (movement_control[3])
				x = -1;
			test_box = box1;
			if (movement_control[7])
				test_box = test_box.rotate(0.1);
			if (movement_control[8])
				test_box = test_box.rotate(-0.1);
			move(test_box, x, y);
			if (x != 0 || y != 0) //Collision messages.
			{
				/*if (tmp_pudlo1.isCollision(pudlo2))
				 {
				 //std::cout << " Kolizja z pudlo2! " << pudlo1.p[4].x << " "
				 //		<< pudlo1.p[4].y << std::endl;
				 collision = true;
				 }
				 //std::cout << pudlo3.is_rotated << std::endl;
				 if( tmp_pudlo1.isCollision(pudlo2))
				 {
				 collision = true;
				 }*/
				if (triangle.isCollision(test_box))
				{
					collision = true;
				}

				/*if (collisionBBoxBBox2(tmp_pudlo1, krecha.box tmp_pudlo1.isCollision(krecha.box))
				 {*/
				/*	MaskLine new_krecha(tmp_pudlo1.p[0], tmp_pudlo1.p[1]);
				 //std::cout << " Kolizja z pudlem krechy! " << pudlo1.p[4].x << " " << pudlo1.p[4].y << std::endl;
				 if (collisionLineLine(new_krecha, krecha))
				 {
				 //std::cout << " Kolizja z krecha! " << pudlo1.p[4].x
				 //		<< " " << pudlo1.p[4].y << std::endl;
				 collision = true;

				 }*/
			}
			if (collisionRBoxRBox(test_box, box3))
				collision = true;
			if (!collision)
			{
				if (movement_control[7])
					box1 = box1.rotate(0.1);
				if (movement_control[8])
					box1 = box1.rotate(-0.1);
				move(box1, x, y);
			}
			else
				test_box = box1;
		}

		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
				movement_control[0] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
				movement_control[1] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
				movement_control[2] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
				movement_control[3] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_H)
				movement_control[4] = true;
			if (ev.keyboard.keycode == ALLEGRO_KEY_R)
				movement_control[5] = true;
			if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				movement_control[6] = true;

			if (ev.keyboard.keycode == ALLEGRO_KEY_Q)
				movement_control[7] = true;
			if (ev.keyboard.keycode == ALLEGRO_KEY_W)
				movement_control[8] = true;
		}

		else if (ev.type == ALLEGRO_EVENT_KEY_UP)
		{
			if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
				movement_control[0] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
				movement_control[1] = false;

			if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
				movement_control[2] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
				movement_control[3] = false;

			if (ev.keyboard.keycode == ALLEGRO_KEY_H)
				movement_control[4] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_R)
				movement_control[5] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				movement_control[6] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_Q)
				movement_control[7] = false;
			if (ev.keyboard.keycode == ALLEGRO_KEY_W)
				movement_control[8] = false;
		}

		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			break;

		if (redraw && al_is_event_queue_empty(event_queue))
		{
			redraw = false;
			al_clear_to_color(al_map_rgb(0, 0, 0));
			al_draw_bitmap(map, 0, 0, 0);

			//Draws objects.
			test_box.draw();
			box1.draw();
			box2.draw();
			box3.draw();
			line.box.draw();
			line.normal.draw();
			line.draw();
			edge1.draw();
			edge2.draw();
			edge3.draw();

			al_flip_display();
		}
	}
	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_bitmap(map);
	return 0;
}

int menuLoop(ALLEGRO_DISPLAY * display)
{
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_BITMAP *background = NULL, *cursor_bmp = NULL;
	ALLEGRO_MOUSE_CURSOR *cursor = NULL;

	int for_return = 0;

	bool ster[7] =
	{ false, false, false, false, false, false, false };

	event_queue = al_create_event_queue();
	if (!event_queue)
		return -1;

	timer = al_create_timer(1.0 / FPS);
	if (!timer)
		return -2;

	background = al_load_bitmap("assets/map1.png");
	if (!background)
		return -3;

	//Cursor for main menu.
	cursor_bmp = al_load_bitmap("assets/cursor.png");
	if (!cursor_bmp)
		return -4;
	cursor = al_create_mouse_cursor(cursor_bmp, 3, 11);
	if (!cursor)
		return -5;
	al_set_mouse_cursor(display,cursor);

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_mouse_event_source());

	bool redraw = false;

	al_start_timer(timer);

	while (1)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			redraw = true;
			if (ster[0])
			{
				for_return = 1;
				break;
			}
			if (ster[1])
			{
				for_return = 2;
				break;
			}
			if (ster[2])
			{
				for_return = 0;
				break;
			}
			if (ster[3])
			{
				for_return = 3;
				break;
			}

		}

		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			if (ev.keyboard.keycode == ALLEGRO_KEY_P)
				ster[0] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_V)
				ster[1] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				ster[2] = true;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_O)
				ster[3] = true;
		}

		else if (ev.type == ALLEGRO_EVENT_KEY_UP)
		{
			if (ev.keyboard.keycode == ALLEGRO_KEY_P)
				ster[0] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_V)
				ster[1] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				ster[2] = false;
			else if (ev.keyboard.keycode == ALLEGRO_KEY_O)
				ster[3] = false;

		}

		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			break;


		if (redraw && al_is_event_queue_empty(event_queue))
		{
			redraw = false;
			al_draw_bitmap(background, 0, 0, 0);
			al_flip_display();
		}

	}
	al_destroy_bitmap(background);
	al_destroy_event_queue(event_queue);
	al_destroy_timer(timer);
	al_destroy_mouse_cursor(cursor);
	al_destroy_bitmap(cursor_bmp);

	return for_return;

}
