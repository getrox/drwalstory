#ifndef PLAYER_H
#define PLAYER_H

#include "Drwal.h"
#include "collision.h"
class player
{
    public:
        ALLEGRO_BITMAP *drwal_image;
        float pos_x, pos_y,s_x,s_y;         //For player position, and speed
        player();
        BBox drwal_box;                     //Collision box
        bool jump;

        void sterowanie(bool *ster, MapMask *Map);              //Player movement, AND collision!
        void sterowanie2(bool *ster, MapMask *Map);
        void sterowanie3(MapMask *Map);
        void debug_ster(bool *ster, MapMask *Map);

        void acceleration(bool *ster);                          //Player acceleration, collects info from ster array
        void pokaz();

};

#endif
