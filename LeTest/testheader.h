struct MapMask
{
    int** Mask;
    int size_w,size_h;
};

struct BBox{
      float x1, y1, x2, y2;
      int width, height;    //Might be useless.
};

MapMask loadBitmap(ALLEGRO_BITMAP* Bitmap);
void c_showBitmap(MapMask *Map);
bool collisionBBoxMask(BBox * box, MapMask * Map);
bool collisionBBoxMask_w_cords(BBox * box, MapMask * Map, int *max_y, int *max_x, int *min_y, int *min_x);
void draw_from_Mask(MapMask *Map);

inline ALLEGRO_COLOR colorSet(int r, int g, int b, int a, ALLEGRO_COLOR *col = NULL)
{
    col = new ALLEGRO_COLOR;
    col->r = r/255;
    col->g = g/255;
    col->b = b/255;
    col->a = a/255;
    return *col;
}

inline bool colorCompare(int r, int g, int b, int a, ALLEGRO_COLOR px)
{

    //std::cout << px.r << " " << px.g << " " << px.r << std::endl;
    if(r/255 == px.r && g/255 == px.g && b/255 == px.b /*&& a/255 == px.a*/)
        return true;
    else
        return false;
}

inline BBox copyBBox(BBox *box)
{
    BBox newBox;
    newBox.height = box->height;
    newBox.width  = box->width;
    newBox.x1 = box->x1;
    newBox.x2 = box->x2;
    newBox.y1 = box->y1;
    newBox.y2 = box->y2;

    return newBox;
}

inline BBox moveBBox(BBox *box, int x, int y)
{
    BBox newBox;
    newBox.height = box->height;
    newBox.width  = box->width;
    newBox.x1 = box->x1 + x;
    newBox.x2 = box->x2 + x;
    newBox.y1 = box->y1 + y;
    newBox.y2 = box->y2 + y;
    return newBox;
}


