#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "allegro5/allegro_native_dialog.h"
#include <allegro5/allegro_primitives.h>
#include <iostream>
#include "testheader.h"


const float FPS = 60;

class player
{
    public:
        ALLEGRO_BITMAP *drwal_image;
        float pos_x, pos_y,s_x,s_y;         //For player position, and speed
        player();
        BBox drwal_box;                     //Collision box
        bool jump;

        void sterowanie(bool *ster, MapMask *Map);              //Player movement, AND collision!
        void sterowanie2(bool *ster, MapMask *Map);
        void sterowanie3(MapMask *Map);
        void debug_ster(bool *ster, MapMask *Map);

        void acceleration(bool *ster);                          //Player acceleration, collects info from ster array
        void pokaz();

};

player::player()                                                //General player init.
{
    s_x = 0;                //Velocity stuff
    s_y = 0;
    jump = false;           //Flag for jumping

    pos_x = 50;             //Starting pos
    pos_y = 50;
                            // Le image
    drwal_image = al_load_bitmap("assets/drwal.png");

    if (!drwal_image)
        std::cout << "Couldn't load yer axeman. Ye got yer files right ?" << drwal_image << std::endl;

    //setting our bounding box.
    drwal_box.width = al_get_bitmap_width(drwal_image)-14;
    drwal_box.height = al_get_bitmap_height(drwal_image)-6;
    drwal_box.x1 = pos_x+7;
    drwal_box.y1 = pos_y+3;
    drwal_box.x2 = pos_x+7 + drwal_box.width;
    drwal_box.y2 = pos_y+3 + drwal_box.height;

}

void player::sterowanie(bool *ster,MapMask *Map)        //My second attempt. Tried to get the farthest pixel coords
{                                                       //from the mask, which we use to move our character.
    BBox tmp;                                           //Less calculations, but probably I've messed up the function
    float x=0,y=0;                                      //which does that, so there are a few bugs.
    int max_x=0,min_x=0,max_y=0,min_y=0;
    tmp = copyBBox(&drwal_box);
    bool can_move = true;

    if (ster[0])
        y -= 5;

    if (ster[1])
        y += 5;

    if (ster[2])
        x += 5;

    if (ster[3])
        x -= 5;

    can_move = !collisionBBoxMask_w_cords(& moveBBox(&tmp,x,y), Map, &max_y, &max_x, &min_y, &min_x);

    if(!can_move)
    {
        //std::cout << "min_x: " << min_x << " x2: " << drwal_box.x2 << "\n"
        //          << "max_x: " << max_x << " x1: " << drwal_box.x1 << "\n"
        //          << "max_y: " << max_y << " y2: " << drwal_box.y2 << "\n"
        //          << "min_y: " << min_y << " y1: " << drwal_box.y1 << "\n"
        //          << std::endl;

        if(x > 0)
            x = min_x - drwal_box.x2-1;
        else if(x < 0)
            x = max_x - drwal_box.x1+1;


        if(y > 0)
            y = min_y - drwal_box.y2-1;
        else if(y < 0)
            y = max_y - drwal_box.y1+1;



        //std::cout << "x: " << x << " y: " << y << std::endl;
        if (x != 0 || y != 0)
            can_move = !collisionBBoxMask(& moveBBox(&tmp,x,y), Map);
    }
    if(can_move)
    {
        pos_x += x;
        pos_y += y;
        drwal_box = moveBBox(&drwal_box,x,y);
    }



}

void player::sterowanie2(bool *ster,MapMask *Map)   //Generally tries to first collect where you'd like to move.
{                                                   //And then check pixel by pixel if that's possible. No real difference
    BBox tmp;                                       //between x an y axis. Works, but quite buggy.
    float x=0,y=0;
    tmp = copyBBox(&drwal_box);

    bool can_move = true;
    if (ster[0])
        y -= 10;

    if (ster[1])
        y += 10;

    if (ster[2])
        x += 10;


    if (ster[3])
        x -= 10;

    can_move = !collisionBBoxMask(& moveBBox(&tmp,x,y), Map);

    if(!can_move)
    {

        if (y > 0)
        {
            for(int i = 1; i < y && !can_move; i++)
            {
                can_move = !collisionBBoxMask(&moveBBox(&tmp,x,i), Map);
                if(can_move)
                    y = i;
            }
        }
        else if (y < 0)
        {
            for(int i = -1; i > y; i--)
            {
                can_move = !collisionBBoxMask(&moveBBox(&tmp,x,i), Map);
                if(can_move)
                    y = i;
            }
        }

        if (x > 0)
        {
            for(int j = 1; j < x; j++)
            {
                can_move = !collisionBBoxMask(&moveBBox(&tmp,j,y), Map);
                if(can_move)
                    x = j;
            }

        }
        else if (x < 0)
        {
            for(int j = -1; j > x; j++)
            {
                can_move = !collisionBBoxMask(&moveBBox(&tmp,j,y), Map);
                if(can_move)
                    x = j;
            }
        }

    }
    if(can_move)
    {
        pos_x += x;
        pos_y += y;
        drwal_box = moveBBox(&drwal_box,x,y);
    }



}


void player::sterowanie3(MapMask *Map)                  //Safe, but not optimized. Tho the character can get stuck.
{
    BBox tmp;                                           //Temporary box, for checking where can we go.
    int sol_x[8] = {1, 1, 1, 1, 1, 1, 1, 2},            //Solution for movement -> where can the player go, 2 pixels east and 1 or 7 up?
        sol_y[8] = {0,-1,-2,-3,-4,-5,-6,-7};
    int solution = 0, i = 1;                            //For indexing the solution stuff, see for loops.
    int max_x = abs(s_x),max_y = abs(s_y);              //The max movement, considering current velocity.
    //jump = false;

    tmp = copyBBox(&drwal_box);
    bool can_move = false;

    if (s_y < 0)                                        //Move up( jump)
    {
        i = 1;
        while ( i <= max_y)                             //Checking all the pixels <= max velocity for y
        {
            tmp = copyBBox(&drwal_box);
            can_move = !collisionBBoxMask(&moveBBox(&tmp, 0, -1),Map);      //Can we go 1 pixel up ?
            if(can_move)                                                    //Yes, we can! Go 1 pixel up!
            {

                i++;
                pos_y -= 1;
                drwal_box = moveBBox(&drwal_box, 0, -1);
                //std::cout << pos_y << std::endl;
            }
            else                                                            //No, you can't. No point in checking more.
            {
                s_y = s_y/2;
                break;
            }
        }
    }

    else if (s_y > 0)                                   //Move down. Rest is analogical for move up stuff.
    {
        i = 1;
        while ( i <= max_y)
        {
            can_move = false;
            tmp = copyBBox(&drwal_box);
            can_move = !collisionBBoxMask(&moveBBox(&tmp, 0, 1),Map);
            if(can_move)
            {
                i++;
                pos_y += 1;
                drwal_box = moveBBox(&drwal_box, 0, 1);
            }
            else
            {
                s_y = 0;
                break;
            }
        }
    }
    if (s_x > 0)                                                        //Move left.
    {
        i = 1;
        while ( i <= max_x)                                             //To check all the pixels before me
        {
            can_move = false;
            tmp = copyBBox(&drwal_box);                                 //reseting for each movement.
            for(solution = 0; !can_move && solution <= 7; 1)            //Checking all solutions
            {
                can_move = !collisionBBoxMask(&moveBBox(&tmp, sol_x[solution], sol_y[solution]), Map);  //Can i move with solution nr X ?
                if(!can_move)
                    solution += 1;
            }
            if(can_move)                            //Yes I can!
            {
                if(solution == 7)                   //If using solution nr 7, you go two pixels further.
                    i += 2;
                else                                //If not, you just go 1 pixel.
                    i += 1;

                pos_x += sol_x[solution];           //Moove!
                pos_y += sol_y[solution];
                drwal_box = moveBBox(&drwal_box, sol_x[solution], sol_y[solution]);
            }
            else
                break;
        }
    }

    else if (s_x < 0)                                       //Move left. Analogical. We use "-" before sol'ution'_x to move left.
    {
        i = 1;
        while ( i <= max_x)
        {
            can_move = false;
            tmp = copyBBox(&drwal_box);
            for(solution = 0; !can_move && solution <= 7; 1)
            {

                can_move = !collisionBBoxMask(&moveBBox(&tmp, -sol_x[solution], sol_y[solution]), Map);
                if(!can_move)
                    solution += 1;
            }
            if(can_move)
            {
                if(solution == 7)
                    i += 2;
                else
                    i += 1;

                pos_x += -sol_x[solution];
                pos_y += sol_y[solution];
                drwal_box = moveBBox(&drwal_box, -sol_x[solution], sol_y[solution]);
            }
            else
                break;
        }
    }

    if (collisionBBoxMask(&moveBBox(&tmp,0,5),Map))
        jump = true;
    else jump = false;

}

void player::debug_ster(bool *ster, MapMask *Map)               //For debugging purposes. We move everywhere. Like a ghost.
{

    float x=0,y=0;
    bool can_move = true;

    if (ster[0])
        s_y -= 0.25;

    else if (ster[1])
        s_y += 0.25;

    else
        s_y /= 1.5;

    if (ster[2])
        s_x += 0.25;

    else if (ster[3])
        s_x -= 0.25;

    else
        s_x /= 1.5;


    x = s_x;
    y = s_y;

    pos_x += x;
    pos_y += y;
    drwal_box = moveBBox(&drwal_box,x,y);

    if( collisionBBoxMask(&drwal_box, Map ) )
        std::cout << "Kolizja! " << drwal_box.x1 << " " << drwal_box.x2 << " "
                                 << drwal_box.y1 << " " << drwal_box.y2 << "\n" << std::endl;



}

void player::acceleration(bool *ster)               //When you'd like to mess up with velocity... try this.
{
    if (!jump)
    {
        s_y += 0.5;
        /*if (s_y > 12.5)
            s_y = 12.5;*/
    }
    else
    {
        if(ster[0])
            s_y = -8;

        if(ster[2])
        {
            s_x += 0.25;
            if (s_x > 5)
                s_x = 5;
        }
        else if(ster[3])
        {
            s_x -= 0.25;
            if (s_x < -5)
                s_x = -5;
        }
        else
            s_x /= 1.5;
    }
}

int main(int argc, char ** argv)
{
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;

    ALLEGRO_BITMAP *image = NULL;           //Mask image, and viewable image
    ALLEGRO_BITMAP *hejoo = NULL;

    ALLEGRO_TIMER *timer = NULL;
    ALLEGRO_COLOR lecolor;

    MapMask Map;

    al_init_primitives_addon();

    bool ster[6] =                          //For character controling
    { false, false, false, false,false,false };
    bool toggle_mask = false;

    if (!al_init())
        return -1;

    display = al_create_display(800, 640);
    if (!display)
        return -2;

    if (!al_install_keyboard())
    {
        return -1;
    }

    event_queue = al_create_event_queue();
    if (!event_queue)
        return -3;

    timer = al_create_timer(1.0 / FPS);
    if (!timer)
    {
        std::cout << "failed to create timer!" << std::endl;
        return -1;
    }

    al_init_image_addon();                                  //Images!
    hejoo = al_load_bitmap("assets/map1.png");
    image = al_load_bitmap("assets/collisionmap1.png");
    if (!image)
    {
        //al_show_native_message_box(display, "Error", "Error", "Failed to load image!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        std::cout << "Move dir 'assets' to the folder with this file. It works different for me..." << std::endl;
        al_destroy_display(display);
        return -100;
    }
    Map = loadBitmap(image);
    //c_showBitmap(&Map);

    lecolor = colorSet(255,0,0,100);            //The color for lines around the character
                                                    //colorSet isn't needed. al_map_rgb or something should work.

    player gracz[1];
    //al_set_target_bitmap(gracz.drwal_image);
    al_set_target_bitmap(al_get_backbuffer(display));

    draw_from_Mask(&Map);                           //Drawing mask for a second.
    al_flip_display();
    //c_showBitmap(&Map);
    al_rest(1.0);

    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));

    al_register_event_source(event_queue, al_get_keyboard_event_source());

    al_start_timer(timer);

    bool redraw = false;

    while (1)                                   //Main loop. I'll put it in a seperate function later.
    {
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);
        if (ev.type == ALLEGRO_EVENT_TIMER)     //When it's the 1/60 of the second.
        {
            redraw = true;
            for (int i = 0; i < 1; i++)
            {
            gracz[i].acceleration(ster);
            gracz[i].sterowanie3(&Map);
            if(ster[5])
            {
                gracz[i].drwal_box = moveBBox(&gracz[i].drwal_box, 50 - gracz[i].pos_x,50 - gracz[i].pos_y);
                gracz[i].pos_x = 50;
                gracz[i].pos_y = 50;
            }
            }
            if(ster[4])
            {
                if(toggle_mask)
                    toggle_mask = false;
                else
                    toggle_mask = true;
            }

            //gracz[0].debug_ster(ster, &Map);

        }

        else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)     //Hold the key down -> get trues -> move yer character!
        {
            if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
                //std::cout << "LOL" << std::endl;
                ster[0] = true;
            else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
                ster[1] = true;

            if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
                ster[2] = true;
            else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
                ster[3] = true;

            if(ev.keyboard.keycode == ALLEGRO_KEY_H)
                ster[4] = true;
            if (ev.keyboard.keycode == ALLEGRO_KEY_R)
                ster[5] = true;

        }

        else if (ev.type == ALLEGRO_EVENT_KEY_UP)       //Leave the key alone -> get fales -> your character is
        {                                               //from you, you sadist!
            if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
            {
                //std::cout << "LOL" << std::endl;
                ster[0] = false;
            }
            else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
                ster[1] = false;

            if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
                ster[2] = false;
            else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
                ster[3] = false;

            if(ev.keyboard.keycode == ALLEGRO_KEY_H)
                ster[4] = false;
            if (ev.keyboard.keycode == ALLEGRO_KEY_R)
                ster[5] = false;
        }

        else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)    //Getting out from this infinity loop.
        {
            break;
        }

        if (redraw && al_is_event_queue_empty(event_queue)) //No events, and we need to redraw ? Let's get to work!
        {
            redraw = false;                                 //Not optimalized.
            al_clear_to_color(al_map_rgb(0, 0, 0));
            if(!toggle_mask)
                al_draw_bitmap(hejoo, 0, 0, 0);
            else
                al_draw_bitmap(image,0,0,0);
                //draw_from_Mask(&Map);

            for(int i = 0; i< 1; i++)
            al_draw_bitmap(gracz[i].drwal_image, gracz[i].pos_x, gracz[i].pos_y, 0);
            //////////DRAW LINES////////// Checking if they go after the bounding box
            al_draw_line( gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y1, gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y1, lecolor,0);
            al_draw_line( gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y1, gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y2, lecolor,0);
            al_draw_line( gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y1, gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y2, lecolor,0);
            al_draw_line( gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y2, gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y2, lecolor,0);
            al_draw_pixel(gracz[0].drwal_box.x1, gracz[0].drwal_box.y1, lecolor);
            al_draw_pixel(gracz[0].drwal_box.x1, gracz[0].drwal_box.y2, lecolor);
            al_draw_pixel(gracz[0].drwal_box.x2, gracz[0].drwal_box.y1, lecolor);
            al_draw_pixel(gracz[0].drwal_box.x2, gracz[0].drwal_box.y2, lecolor);

            al_flip_display();
        }
        //al_clear_to_color(al_map_rgb(0,0,0));
        al_flip_display();
    }

    al_destroy_display(display);

    return 0;
}

