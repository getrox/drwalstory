#ifndef COLLISION_H
#define COLLISION_H

#include "Drwal.h"

struct MapMask
{
    int** Mask;
    int size_w,size_h;
};

struct BBox{
      float x1, y1, x2, y2;
      int width, height;    //Might be useless.
};

inline BBox copyBBox(BBox *box)
{
    BBox newBox;
    newBox.height = box->height;
    newBox.width  = box->width;
    newBox.x1 = box->x1;
    newBox.x2 = box->x2;
    newBox.y1 = box->y1;
    newBox.y2 = box->y2;

    return newBox;
}

inline BBox moveBBox(BBox *box, int x, int y)
{
    BBox newBox;
    newBox.height = box->height;
    newBox.width  = box->width;
    newBox.x1 = box->x1 + x;
    newBox.x2 = box->x2 + x;
    newBox.y1 = box->y1 + y;
    newBox.y2 = box->y2 + y;
    return newBox;
}

MapMask loadBitmap(ALLEGRO_BITMAP* Bitmap);
void c_showBitmap(MapMask *Map);
bool collisionBBoxMask(BBox * box, MapMask * Map);
bool collisionBBoxMask_w_cords(BBox * box, MapMask * Map, int *max_y, int *max_x, int *min_y, int *min_x);
void draw_from_Mask(MapMask *Map);
#endif
