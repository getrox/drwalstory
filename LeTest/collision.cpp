#include "Drwal.h"
#include "collision.h"
#include "other_stuff.h"

MapMask loadBitmap(ALLEGRO_BITMAP* Bitmap)
{
    al_lock_bitmap(Bitmap, al_get_bitmap_format(Bitmap), ALLEGRO_LOCK_READONLY);
    MapMask newBitmap;
    newBitmap.size_w = al_get_bitmap_width(Bitmap);
    newBitmap.size_h = al_get_bitmap_height(Bitmap);
    std::cout << newBitmap.size_w << " " << newBitmap.size_h << std::endl;
    newBitmap.Mask = new int*[newBitmap.size_h];
    for (int i = 0; i < newBitmap.size_h; i++)
    {
        newBitmap.Mask[i] = new int[newBitmap.size_w];

        for (int j = 0; j < newBitmap.size_w; j++)
        {
            if (colorCompare(255, 255, 255, 255, al_get_pixel(Bitmap, j, i)))
                newBitmap.Mask[i][j] = 0;
            else
                newBitmap.Mask[i][j] = 1;
        }
    }
    al_unlock_bitmap(Bitmap);
    return newBitmap;
}

void c_showBitmap(MapMask *Map)
{
    int h = Map->size_h, w = Map->size_w;
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
            std::cout << Map->Mask[i][j] << " ";
        std::cout << std::endl;
    }
}

bool collisionBBoxMask(BBox * box, MapMask * Map)
{
    bool Collision = false;

    int x1 = box->x1, x2 = box->x2, y1 = box->y1, y2 = box->y2; //For indexing the Mask array.
    int i, j;

    //Checking if the bounding box is outside the array.
    //std::cout << y2 << " " << Map->size_h << std::endl;
    if (x1 < 0)
        x1 = 0;
    if (y1 < 0)
        y1 = 0;

    if (x2 < 0) //This shouldn't happen.
        x2 = 0;
    if (y2 < 0)
        y2 = 0;

    if (x2 >= Map->size_w)
        x2 = Map->size_w - 1;
    if (y2 >= Map->size_h)
        y2 = Map->size_h - 1;

    if (x1 >= Map->size_w) //And again, this shouldn't happen. No character totally out of the map.
        x1 = Map->size_w - 1;
    if (y1 >= Map->size_h) //Eventually if any of these is true, Collision = true and return!
        y1 = Map->size_h - 1; //... maybe except flying out of the screen in the sky, though.

    i = x1;
    j = y1;

    //std::cout << "Zyje" << std::endl;

    while (i <= x2 && !Collision) //Checking both up and down border
    {
        //std::cout << "i: " << y1 << " x1: " << y2 << std::endl;
        if (Map->Mask[y1][i] == 1 || Map->Mask[y2][i])
        {
            Collision = true;
            //std::cout << "findo "<< j << " " << i << std::endl;
        }
        i++;
    }

    //std::cout << "Zyje2" << std::endl;
    while (j <= y2 && !Collision) //Checking both left and right borders
    {
        if (Map->Mask[j][x1] == 1 || Map->Mask[j][x2] == 1)
        {
            Collision = true;
            //std::cout << "findor "<< j << " " << i << std::endl;
        }
        j++;
    }
    //std::cout << "Zyje3" << std::endl;

    if (!Collision) //Checking for the whole area if collision not found.
    {
        i = x1 + 1;
        while (i < x2 && !Collision)
        {
            j = y1 + 1;
            while (j < y2 && !Collision)
            {
                if (Map->Mask[j][i] == 1)
                {
                    Collision = true;
                    std::cout << "find " << j << " " << i << std::endl;
                }
                j++;
            }
            i++;
        }
    }
    //std::cout << "Zyje4" << std::endl;
    return Collision;

}

bool collisionBBoxMask_w_cords(BBox * box, MapMask * Map, int *max_y,
        int *max_x, int *min_y, int *min_x)
{
    bool Collision = false;
    bool in_column = false, in_row = false, first_row = true, first_column =
            true;

    int x1 = box->x1, x2 = box->x2, y1 = box->y1, y2 = box->y2; //For indexing the Mask array.
    int i, j;

    *min_y = y2; //Making the variables go somewhere.
    *min_x = x2;
    *max_y = y1;
    *max_x = x1;

    //Checking if the bounding box is outside the array.

    if (x1 < 0)
        x1 = 0;
    if (y1 < 0)
        y1 = 0;

    if (x2 < 0) //This shouldn't happen.
        x2 = 0;
    if (y2 < 0)
        y2 = 0;

    if (x2 > Map->size_w)
        x2 = Map->size_w - 1;
    if (y2 > Map->size_h)
        y2 = Map->size_h - 1;

    if (x1 > Map->size_w) //And again, this shouldn't happen. No character totally out of the map.
        x1 = Map->size_w - 1;
    if (y1 > Map->size_h) //Eventually if any of these is true, Collision = true and return!
        y1 = Map->size_h - 1; //... maybe except flying out of the screen in the sky, though.

    i = x1;
    j = y1;

    while (i <= x2)
    {
        in_column = false;
        j = y1;
        while (j <= y2)
        {
            in_row = false;
            //std::cout << Map->Mask[i][j] << " ";
            if (Map->Mask[j][i] == 1)
            {
                Collision = true;
                in_column = true;
                in_row = true;

            }
            if (in_column)
            {
                if (*min_x > i)
                    *min_x = i;
                if (*max_x < i)
                    *max_x = i;
            }
            if (in_row)
            {
                if (*min_y > j)
                    *min_y = j;
                //first_row = false;
                if (*max_y < j)
                    *max_y = j;
            }

            j++;
        }
        //std::cout << std::endl;
        i++;
    }
    //std::cout << "Zyje3" << std::endl;
    return Collision;

}

void draw_from_Mask(MapMask *Map) //For debugging purpose
{
    ALLEGRO_COLOR color;
    color = al_map_rgba(0, 0, 255, 255);
    int w = Map->size_w, h = Map->size_h;

    //std::cout << w << " " << h << std::endl;

    for (int i = 0; i < h; i++)
    {
        //std::cout << i << "!" << std::endl;

        for (int j = 0; j < w; j++)
        {
            //std::cout << j << "?" << std::endl;

            if (Map->Mask[i][j] == 1)
                al_draw_pixel(j, i + 1, color);
        }
    }
}

