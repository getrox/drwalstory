#include "Drwal.h"
#include "collision.h"
#include "Player.h"
#include "other_stuff.h"


int main(int argc, char ** argv)
{
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;

    ALLEGRO_BITMAP *image = NULL;           //Mask image, and viewable image
    ALLEGRO_BITMAP *hejoo = NULL;

    ALLEGRO_TIMER *timer = NULL;
    ALLEGRO_COLOR lecolor;

    MapMask Map;

    al_init_primitives_addon();

    bool ster[6] =                          //For character controling
    { false, false, false, false,false,false };
    bool toggle_mask = false;

    if (!al_init())
        return -1;

    display = al_create_display(800, 640);
    if (!display)
        return -2;

    if (!al_install_keyboard())
    {
        return -1;
    }

    event_queue = al_create_event_queue();
    if (!event_queue)
        return -3;

    timer = al_create_timer(1.0 / FPS);
    if (!timer)
    {
        std::cout << "failed to create timer!" << std::endl;
        return -1;
    }

    al_init_image_addon();                                  //Images!
    hejoo = al_load_bitmap("assets/map1.png");
    image = al_load_bitmap("assets/collisionmap1.png");
    if (!image)
    {
        //al_show_native_message_box(display, "Error", "Error", "Failed to load image!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        std::cout << "Move dir 'assets' to the folder with this file. It works different for me..." << std::endl;
        al_destroy_display(display);
        return -100;
    }
    Map = loadBitmap(image);
    //c_showBitmap(&Map);

    lecolor = al_map_rgba(255,0,0,100);            //The color for lines around the character
                                                    //colorSet isn't needed. al_map_rgb or something should work.

    player gracz[1];
    //al_set_target_bitmap(gracz.drwal_image);
    al_set_target_bitmap(al_get_backbuffer(display));

    draw_from_Mask(&Map);                           //Drawing mask for a second.
    al_flip_display();
    //c_showBitmap(&Map);
    al_rest(1.0);

    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));

    al_register_event_source(event_queue, al_get_keyboard_event_source());

    al_start_timer(timer);

    bool redraw = false;

    while (1)                                   //Main loop. I'll put it in a seperate function later.
    {
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);
        if (ev.type == ALLEGRO_EVENT_TIMER)     //When it's the 1/60 of the second.
        {
            redraw = true;
            for (int i = 0; i < 1; i++)
            {
            gracz[i].acceleration(ster);
            gracz[i].sterowanie3(&Map);
            if(ster[5])
            {
                gracz[i].drwal_box = moveBBox(&gracz[i].drwal_box, 50 - gracz[i].pos_x,50 - gracz[i].pos_y);
                gracz[i].pos_x = 50;
                gracz[i].pos_y = 50;
            }
            }
            if(ster[4])
            {
                if(toggle_mask)
                    toggle_mask = false;
                else
                    toggle_mask = true;
            }

            //gracz[0].debug_ster(ster, &Map);

        }

        else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)     //Hold the key down -> get trues -> move yer character!
        {
            if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
                //std::cout << "LOL" << std::endl;
                ster[0] = true;
            else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
                ster[1] = true;

            if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
                ster[2] = true;
            else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
                ster[3] = true;

            if(ev.keyboard.keycode == ALLEGRO_KEY_H)
                ster[4] = true;
            if (ev.keyboard.keycode == ALLEGRO_KEY_R)
                ster[5] = true;

        }

        else if (ev.type == ALLEGRO_EVENT_KEY_UP)       //Leave the key alone -> get fales -> your character is
        {                                               //from you, you sadist!
            if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
            {
                //std::cout << "LOL" << std::endl;
                ster[0] = false;
            }
            else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
                ster[1] = false;

            if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
                ster[2] = false;
            else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
                ster[3] = false;

            if(ev.keyboard.keycode == ALLEGRO_KEY_H)
                ster[4] = false;
            if (ev.keyboard.keycode == ALLEGRO_KEY_R)
                ster[5] = false;
        }

        else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)    //Getting out from this infinity loop.
        {
            break;
        }

        if (redraw && al_is_event_queue_empty(event_queue)) //No events, and we need to redraw ? Let's get to work!
        {
            redraw = false;                                 //Not optimalized.
            al_clear_to_color(al_map_rgb(0, 0, 0));
            if(!toggle_mask)
                al_draw_bitmap(hejoo, 0, 0, 0);
            else
                al_draw_bitmap(image,0,0,0);
                //draw_from_Mask(&Map);

            for(int i = 0; i< 1; i++)
            al_draw_bitmap(gracz[i].drwal_image, gracz[i].pos_x, gracz[i].pos_y, 0);
            //////////DRAW LINES////////// Checking if they go after the bounding box
            al_draw_line( gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y1, gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y1, lecolor,0);
            al_draw_line( gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y1, gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y2, lecolor,0);
            al_draw_line( gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y1, gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y2, lecolor,0);
            al_draw_line( gracz[0].drwal_box.x1+1, gracz[0].drwal_box.y2, gracz[0].drwal_box.x2+1, gracz[0].drwal_box.y2, lecolor,0);
            al_draw_pixel(gracz[0].drwal_box.x1, gracz[0].drwal_box.y1, lecolor);
            al_draw_pixel(gracz[0].drwal_box.x1, gracz[0].drwal_box.y2, lecolor);
            al_draw_pixel(gracz[0].drwal_box.x2, gracz[0].drwal_box.y1, lecolor);
            al_draw_pixel(gracz[0].drwal_box.x2, gracz[0].drwal_box.y2, lecolor);

            al_flip_display();
        }
        //al_clear_to_color(al_map_rgb(0,0,0));
        al_flip_display();
    }

    al_destroy_display(display);

    return 0;
}
